package io.atherios.atherioshr.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;
import java.util.Optional;

@NoRepositoryBean
public interface AtheriosRepository<ENTITY, ID extends Serializable> extends JpaRepository<ENTITY, ID> {

    default Optional<ENTITY> tryFind(ID id) {
        return Optional.ofNullable(findOne(id));
    }

    ENTITY find(ID id);

}