package io.atherios.atherioshr.persistence;

import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;

import javax.persistence.EntityManager;
import java.io.Serializable;

public class DefaultAtheriosRepository<ENTITY, ID extends Serializable> extends SimpleJpaRepository<ENTITY, ID> implements AtheriosRepository<ENTITY, ID> {

    public DefaultAtheriosRepository(JpaEntityInformation<ENTITY, ID> entityInformation, EntityManager entityManager) {
        super(entityInformation, entityManager);
    }

    @Override
    public ENTITY find(ID id) {
        ENTITY entity = findOne(id);
        if (entity == null)
            throw new EntityNotFoundException(getDomainClass(), id);
        return entity;
    }
}