package io.atherios.atherioshr.persistence;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(basePackages = "io.atherios", enableDefaultTransactions = false, repositoryBaseClass = DefaultAtheriosRepository.class)
public class PersistenceConfig {
}
