package io.atherios.atherioshr.persistence;

import java.io.Serializable;

public class EntityNotFoundException extends RuntimeException {

    private final Class<?> clazz;
    private final Serializable id;

    public EntityNotFoundException(Class<?> clazz, Serializable id) {
        super("Cannot find " + clazz.getSimpleName() + " with id [" + id + ']');
        this.clazz = clazz;
        this.id = id;
    }

    public Class<?> getClazz() {
        return clazz;
    }

    public Serializable getId() {
        return id;
    }
}
