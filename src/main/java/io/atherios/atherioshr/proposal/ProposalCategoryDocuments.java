package io.atherios.atherioshr.proposal;

import com.google.common.primitives.Ints;
import io.atherios.atherioshr.lucene.AtheriosDocument;
import io.atherios.atherioshr.lucene.AtheriosDocuments;
import io.atherios.atherioshr.lucene.LuceneDocumentBuilder;
import io.atherios.atherioshr.lucene.index.IndexFields;
import io.atherios.atherioshr.proposal.states.ProposalState;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.stream.Stream;

@Component
@AllArgsConstructor
public class ProposalCategoryDocuments implements AtheriosDocuments {

    private final ProposalCategoryRepository proposalCategoryRepository;

    @Override
    public Stream<AtheriosDocument> allDocuments() {
        return proposalCategoryRepository.findAll().stream()
                .map(this::asDocument);
    }

    @Override
    public Optional<Object> findSearchHit(String id) {
        return proposalCategoryRepository.tryFind(Ints.tryParse(id))
                .map(proposalCategory -> ProposalCategoryListItem.preconfigure(proposalCategory).build());
    }

    @Override
    public String documentType() {
        return ProposalCategory.class.getCanonicalName();
    }

    @Override
    public Optional<AtheriosDocument> findIndexDocument(String entityId) {
        return proposalCategoryRepository.tryFind(Integer.parseInt(entityId)).map(this::asDocument);
    }

    private AtheriosDocument asDocument(ProposalCategory proposalCategory) {
        return LuceneDocumentBuilder.builder(ProposalCategory.class, proposalCategory.getId())
                .withField(IndexFields.STRINGS_SEARCH, '#' + proposalCategory.getId().toString())
                .withField(IndexFields.TEXT_SEARCH, proposalCategory.getSummary())
                .build();
    }
}
