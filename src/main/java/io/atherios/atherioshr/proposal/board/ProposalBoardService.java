package io.atherios.atherioshr.proposal.board;

import com.google.common.collect.ImmutableMap;
import io.atherios.atherioshr.proposal.ProposalCategory;
import io.atherios.atherioshr.proposal.ProposalCategoryRepository;
import io.atherios.atherioshr.proposal.proposalusers.Proposal;
import io.atherios.atherioshr.proposal.proposalusers.ProposalRepository;
import io.atherios.atherioshr.proposal.states.ProposalState;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

@Service
public class ProposalBoardService {

    private static final Comparator<Proposal> NATURAL_PROPOSALS_ORDER = Comparator.comparing(Proposal::getCreatedDate).reversed();

    private final ProposalRepository proposalRepository;
    private final ProposalCategoryRepository proposalCategoryRepository;

    public ProposalBoardService(ProposalRepository proposalRepository, ProposalCategoryRepository proposalCategoryRepository) {
        this.proposalRepository = proposalRepository;
        this.proposalCategoryRepository = proposalCategoryRepository;
    }

    public JsonProposalBoard showBoard(int proposalCategoryId) {
        ProposalCategory proposalCategory = proposalCategoryRepository.find(proposalCategoryId);

        ImmutableMap.Builder<Integer, List<ProposalListItem>> board = ImmutableMap.builder();
        for (ProposalState state : proposalCategory.getStates()) {
            if (isNull(state)) {
                continue;
            }
            List<ProposalListItem> proposals = proposalRepository.findByProposalCategoryAndState(proposalCategoryId, state.getId()).stream()
                    .sorted(NATURAL_PROPOSALS_ORDER)
                    .map(ProposalBoardService::createProposalListItem)
                    .collect(Collectors.toList());
            board.put(state.getId(), proposals);
        }

        return new JsonProposalBoard(board.build());
    }

    private static ProposalListItem createProposalListItem(Proposal proposal) {
        Set<JsonProposalAttachment> attachments = proposal.getFiles().stream()
                .map(file -> new JsonProposalAttachment(file, "/api/proposal-attachments/" + file.getId() + "/file"))
                .collect(Collectors.toSet());

        return ProposalListItem.preconfigure(proposal)
                .files(attachments)
                .build();
    }

}
