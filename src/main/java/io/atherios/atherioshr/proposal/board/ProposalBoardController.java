package io.atherios.atherioshr.proposal.board;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProposalBoardController {

    private final ProposalBoardService proposalBoardService;

    public ProposalBoardController(ProposalBoardService proposalBoardService) {
        this.proposalBoardService = proposalBoardService;
    }

    @GetMapping("/proposal-categories/{proposalCategoryId}/board")
    public JsonProposalBoard showBoard(@PathVariable("proposalCategoryId") int proposalCategoryId) {
        return proposalBoardService.showBoard(proposalCategoryId);
    }

}
