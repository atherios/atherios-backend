package io.atherios.atherioshr.proposal.board;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import io.atherios.atherioshr.files.FileDetails;
import io.atherios.atherioshr.files.JsonFileDetails;
import lombok.Getter;

@Getter
public class JsonProposalAttachment {

    private final int id;
    @JsonUnwrapped
    private final JsonFileDetails fileDetails;
    private final String link;

    public JsonProposalAttachment(FileDetails fileDetails, String link) {
        this.id = fileDetails.getId();
        this.fileDetails = new JsonFileDetails(fileDetails);
        this.link = link;
    }
}
