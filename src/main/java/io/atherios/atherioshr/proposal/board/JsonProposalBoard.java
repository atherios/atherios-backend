package io.atherios.atherioshr.proposal.board;

import com.google.common.collect.ForwardingMap;

import java.util.List;
import java.util.Map;

public class JsonProposalBoard extends ForwardingMap<Integer, List<ProposalListItem>> {

    private final Map<Integer, List<ProposalListItem>> delegate;

    public JsonProposalBoard(Map<Integer, List<ProposalListItem>> delegate) {
        this.delegate = delegate;
    }

    @Override
    protected Map<Integer, List<ProposalListItem>> delegate() {
        return delegate;
    }
}
