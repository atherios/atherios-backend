package io.atherios.atherioshr.proposal.board;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import io.atherios.atherioshr.auth.user.profile.JsonUserProfile;
import io.atherios.atherioshr.proposal.proposalusers.JsonProposal;
import io.atherios.atherioshr.proposal.proposalusers.Proposal;
import lombok.Builder;
import lombok.Getter;

import java.time.Instant;
import java.util.Set;

@Builder
@Getter
public class ProposalListItem {

    private final int id;
    @JsonUnwrapped
    private final JsonProposal proposal;
    private final JsonUserProfile user;
    private final Instant createdDate;
    private final int categoryId;
    private final Set<JsonProposalAttachment> files;

    public static ProposalListItemBuilder preconfigure(Proposal proposal) {
        return ProposalListItem.builder()
                .id(proposal.getId())
                .categoryId(proposal.getCategory().getId())
                .createdDate(proposal.getCreatedDate())
                .proposal(new JsonProposal(proposal))
                .user(new JsonUserProfile(proposal.getUser()));
    }
}
