package io.atherios.atherioshr.proposal;

import io.atherios.atherioshr.persistence.AtheriosRepository;

public interface ProposalCategoryRepository extends AtheriosRepository<ProposalCategory, Integer> {
}
