package io.atherios.atherioshr.proposal;

import io.atherios.atherioshr.files.FileDetails;
import io.atherios.atherioshr.files.FileDetailsRepository;
import io.atherios.atherioshr.lucene.DocumentChangedEvent;
import io.atherios.atherioshr.lucene.search.DocumentIdentity;
import io.atherios.atherioshr.proposal.proposalusers.Proposal;
import io.atherios.atherioshr.proposal.proposalusers.ProposalRepository;
import io.atherios.atherioshr.proposal.proposalusers.attachments.ProposalAttachmentsDirectory;
import io.atherios.atherioshr.proposal.states.ProposalState;
import io.atherios.atherioshr.proposal.states.ProposalStatesRepository;
import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional
@Validated
@AllArgsConstructor
public class ProposalCategoryService {

    private final ProposalCategoryRepository proposalCategoryRepository;
    private final ProposalStatesRepository proposalStatesRepository;
    private final ProposalRepository proposalRepository;
    private final FileDetailsRepository fileDetailsRepository;
    private final ProposalAttachmentsDirectory proposalAttachmentsDirectory;
    private final ApplicationEventPublisher applicationEventPublisher;

    public List<ProposalCategoryListItem> listAll() {
        return proposalCategoryRepository.findAll().stream()
                .map(this::asCategoryListItem)
                .collect(Collectors.toList());
    }

    public ProposalCategoryListItem findOne(int proposalCategoryId) {
        ProposalCategory proposalCategory = proposalCategoryRepository.find(proposalCategoryId);
        return asCategoryListItem(proposalCategory);
    }

    private ProposalCategoryListItem asCategoryListItem(ProposalCategory proposalCategory) {
        int activeProposals = proposalRepository.countActiveProposals(proposalCategory.getId());
        return ProposalCategoryListItem.preconfigure(proposalCategory)
                .activeProposals(activeProposals)
                .build();
    }

    public void create(@Valid JsonProposalCategory jsonProposalCategory) {
        ProposalCategory proposalCategory = ProposalCategory.builder()
                .summary(jsonProposalCategory.getSummary())
                .build();

        proposalCategoryRepository.save(proposalCategory);
        applicationEventPublisher.publishEvent(new DocumentChangedEvent(new DocumentIdentity(ProposalCategory.class, proposalCategory.getId())));
    }

    public void update(int id, @Valid JsonProposalCategory jsonProposalCategory) {
        ProposalCategory proposalCategory = proposalCategoryRepository.find(id);
        proposalCategory.setSummary(jsonProposalCategory.getSummary());
        applicationEventPublisher.publishEvent(new DocumentChangedEvent(new DocumentIdentity(ProposalCategory.class, proposalCategory.getId())));
    }

    public void delete(int id) {
        Proposal proposal = proposalRepository.findByProposalCategory(id);
        Set<FileDetails> files = proposal.getFiles();
        deleteFiles(files);
        fileDetailsRepository.delete(files);

        ProposalCategory proposalCategory = proposalCategoryRepository.find(id);
        proposalStatesRepository.delete(proposalCategory.getStates());

        proposalCategoryRepository.delete(id);
        applicationEventPublisher.publishEvent(new DocumentChangedEvent(new DocumentIdentity(ProposalCategory.class, proposalCategory.getId())));
    }

    private void deleteFiles(Set<FileDetails> files) {
        for (FileDetails fileDetails : files) {
            Path resolve = proposalAttachmentsDirectory.resolveFile(fileDetails);
            try {
                Files.deleteIfExists(resolve);
            } catch (IOException e) {
                throw new UncheckedIOException(e);
            }
        }
    }

}
