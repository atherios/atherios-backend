package io.atherios.atherioshr.proposal.proposalusers.attachments;

import lombok.AllArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@RequestMapping("/proposal-attachments")
public class ProposalAttachmentController {

    private final ProposalAttachmentService proposalAttachmentService;


    @GetMapping("/{attachmentId}/file")
    public ResponseEntity<Resource> attachmentFile(@PathVariable("attachmentId") int attachmentId) {
        return proposalAttachmentService.findAttachmentFile(attachmentId)
                .map(atheriosFile -> ResponseEntity.ok()
                        .contentType(MediaType.parseMediaType(atheriosFile.getMime()))
                        .body(atheriosFile.getResource()))
                .orElseGet(() -> ResponseEntity.notFound()
                        .build());
    }

}
