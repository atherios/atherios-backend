package io.atherios.atherioshr.proposal.proposalusers.lucene;

import com.google.common.primitives.Ints;
import io.atherios.atherioshr.files.FileDetails;
import io.atherios.atherioshr.lucene.AtheriosDocument;
import io.atherios.atherioshr.lucene.AtheriosDocuments;
import io.atherios.atherioshr.lucene.index.IndexFields;
import io.atherios.atherioshr.lucene.LuceneDocumentBuilder;
import io.atherios.atherioshr.proposal.board.ProposalListItem;
import io.atherios.atherioshr.proposal.proposalusers.Proposal;
import io.atherios.atherioshr.proposal.proposalusers.ProposalRepository;
import io.atherios.atherioshr.proposal.proposalusers.attachments.ProposalAttachmentsDirectory;
import lombok.AllArgsConstructor;
import org.apache.tika.Tika;
import org.apache.tika.exception.TikaException;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Optional;
import java.util.stream.Stream;

@Component
@AllArgsConstructor
public class ProposalDocuments implements AtheriosDocuments {

    private final ProposalRepository proposalRepository;
    private final ProposalAttachmentsDirectory proposalAttachmentsDirectory;
    private final Tika tika;

    @Override
    public Stream<AtheriosDocument> allDocuments() {
        return proposalRepository.findAll().stream()
                .map(this::asDocument);
    }

    private AtheriosDocument asDocument(Proposal proposal) {
        LuceneDocumentBuilder luceneDocumentBuilder = LuceneDocumentBuilder.builder(Proposal.class, proposal.getId())
                .withField(IndexFields.STRINGS_SEARCH, '#' + proposal.getId().toString())
                .withField(IndexFields.STRINGS_SEARCH, proposal.getUser().getUsername())
                .withField(IndexFields.MINOR_TEXT_SEARCH, proposal.getState().getLabel())
                .withField(IndexFields.MINOR_TEXT_SEARCH, proposal.getCategory().getSummary());

        proposal.getFiles()
                .forEach(fileDetails -> luceneDocumentBuilder.withField(IndexFields.MINOR_TEXT_SEARCH, extractText(fileDetails)));

        return luceneDocumentBuilder.build();
    }

    private String extractText(FileDetails details) {
        return proposalAttachmentsDirectory.findFile(details)
                .map(file -> {
                    try {
                        return tika.parseToString(file);
                    } catch (IOException | TikaException e) {
                        return null;
                    }
                })
                .orElse("");
    }

    @Override
    public Optional<Object> findSearchHit(String id) {
        return proposalRepository.tryFind(Ints.tryParse(id))
                .map(proposal -> ProposalListItem.preconfigure(proposal).build());
    }

    @Override
    public String documentType() {
        return Proposal.class.getCanonicalName();
    }

    @Override
    public Optional<AtheriosDocument> findIndexDocument(String entityId) {
        return proposalRepository.tryFind(Integer.parseInt(entityId)).map(this::asDocument);
    }
}
