package io.atherios.atherioshr.proposal.proposalusers.history;

import io.atherios.atherioshr.persistence.AtheriosRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProposalHistoryRepository extends AtheriosRepository<ProposalHistory, Integer> {

    @Query("select h from ProposalHistory h where h.proposal.id = :proposalId")
    List<ProposalHistory> findByProposal(@Param("proposalId") int proposalId);

}
