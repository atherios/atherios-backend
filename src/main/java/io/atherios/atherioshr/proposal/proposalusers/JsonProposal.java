package io.atherios.atherioshr.proposal.proposalusers;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.atherios.atherioshr.files.FileDetails;
import lombok.Getter;

import java.util.Set;
import java.util.stream.Collectors;

@Getter
public class JsonProposal {

    private final Set<Integer> fileIds;
    private final int stateId;

    @JsonCreator
    public JsonProposal(@JsonProperty("file_ids") Set<Integer> fileIds,
                        @JsonProperty("state_id") int stateId) {
        this.fileIds = fileIds;
        this.stateId = stateId;
    }

    public JsonProposal(Proposal proposal) {
        this.fileIds = proposal.getFiles().stream()
                .map(FileDetails::getId)
                .collect(Collectors.toSet());
        this.stateId = proposal.getState().getId();
    }
}
