package io.atherios.atherioshr.proposal.proposalusers;

import io.atherios.atherioshr.proposal.board.ProposalListItem;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
public class UserProposalController {

    private final ProposalService proposalService;

    @GetMapping("/users/current/proposals")
    public List<ProposalListItem> userProposals() {
        return proposalService.currentUserProposals();
    }
}
