package io.atherios.atherioshr.proposal.proposalusers;

import java.util.List;

import io.atherios.atherioshr.persistence.AtheriosRepository;
import io.atherios.atherioshr.proposal.board.ProposalListItem;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ProposalRepository extends AtheriosRepository<Proposal, Integer> {

    @Query("select u from Proposal u where u.category.id = :proposalCategoryId and u.state.id = :stateId")
    List<Proposal> findByProposalCategoryAndState(@Param("proposalCategoryId") int proposalCategoryId, @Param("stateId") int stateId);

    @Query("select u from Proposal u where u.category.id = :proposalCategoryId")
    Proposal findByProposalCategory(@Param("proposalCategoryId") int proposalCategoryId);

    @Query("select u from Proposal u where u.state.id = :stateId")
    List<Proposal> findByState(@Param("stateId") int stateId);

    @Query("select count(distinct p) from Proposal p where p.state.closed = false and p.category.id = :proposalCategoryId")
    int countActiveProposals(@Param("proposalCategoryId") int proposalCategoryId);

    @Query("select p from Proposal p where p.user.username = :username")
    List<Proposal> findByUser(@Param("username") String username);
}
