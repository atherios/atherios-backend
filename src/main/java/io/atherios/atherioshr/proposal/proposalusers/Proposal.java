package io.atherios.atherioshr.proposal.proposalusers;

import io.atherios.atherioshr.auth.user.User;
import io.atherios.atherioshr.files.FileDetails;
import io.atherios.atherioshr.proposal.ProposalCategory;
import io.atherios.atherioshr.proposal.states.ProposalState;
import lombok.*;

import javax.persistence.*;
import java.time.Instant;
import java.util.Objects;
import java.util.Set;

@Entity
@NoArgsConstructor
@Builder
@AllArgsConstructor
@Getter
@Setter
public class Proposal {

    @Id
    @GeneratedValue(generator = "proposals_gen")
    @SequenceGenerator(name = "proposals_gen", sequenceName = "proposals_seq")
    private Integer id;

    @JoinColumn(name = "proposal_category_id")
    @ManyToOne(optional = false)
    private ProposalCategory category;

    @JoinColumn(name = "user_id")
    @ManyToOne(optional = false)
    private User user;

    @Column(name = "created_date", nullable = false)
    private Instant createdDate;

    @JoinColumn(name = "state_id")
    @ManyToOne(optional = false)
    private ProposalState state;

    @OneToMany(fetch = FetchType.EAGER)
    private Set<FileDetails> files;

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;

        if (!(o instanceof Proposal))
            return false;

        Proposal proposal = (Proposal) o;
        return Objects.equals(getId(), proposal.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
