package io.atherios.atherioshr.proposal.proposalusers.history;

import io.atherios.atherioshr.auth.user.User;
import io.atherios.atherioshr.proposal.proposalusers.Proposal;
import io.atherios.atherioshr.proposal.states.ProposalState;
import lombok.*;

import javax.persistence.*;
import java.time.Instant;
import java.util.Objects;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ProposalHistory {

    @Id
    @Column(name = "proposal_history_id")
    @GeneratedValue(generator = "proposal_history_gen")
    @SequenceGenerator(name = "proposal_history_gen", sequenceName = "proposal_history_seq")
    private Integer id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "previous_state_id")
    private ProposalState previousState;

    @ManyToOne(optional = false)
    @JoinColumn(name = "next_state_id")
    private ProposalState nextState;

    @ManyToOne(optional = false)
    @JoinColumn(name = "proposal_id")
    private Proposal proposal;

    @Column(name = "action_time", nullable = false)
    private Instant time;

    @JoinColumn(name = "user_id")
    @ManyToOne(optional = false)
    private User user;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProposalHistory)) {
            return false;
        }
        ProposalHistory that = (ProposalHistory) o;
        return Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
