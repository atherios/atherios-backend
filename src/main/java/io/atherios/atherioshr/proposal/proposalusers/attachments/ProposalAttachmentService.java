package io.atherios.atherioshr.proposal.proposalusers.attachments;

import io.atherios.atherioshr.files.FileDetails;
import io.atherios.atherioshr.files.FileDetailsRepository;
import io.atherios.atherioshr.proposal.categoryattachments.AtheriosFile;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class ProposalAttachmentService {

    private final FileDetailsRepository fileDetailsRepository;
    private final ProposalAttachmentsDirectory proposalAttachmentsDirectory;
    
    public Optional<AtheriosFile> findAttachmentFile(int attachmentId) {
        FileDetails fileDetails = fileDetailsRepository.find(attachmentId);
        return proposalAttachmentsDirectory.findFile(fileDetails)
                .map(file -> new AtheriosFile(file, fileDetails.getMime()));
    }
}
