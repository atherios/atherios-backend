package io.atherios.atherioshr.proposal.proposalusers;

import java.time.Instant;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import javax.validation.Valid;

import io.atherios.atherioshr.auth.AtheriosAuthentication;
import io.atherios.atherioshr.auth.user.User;
import io.atherios.atherioshr.auth.user.UsersRepository;
import io.atherios.atherioshr.files.FileDetails;
import io.atherios.atherioshr.files.FileDetailsRepository;
import io.atherios.atherioshr.files.FileService;
import io.atherios.atherioshr.lucene.DocumentChangedEvent;
import io.atherios.atherioshr.lucene.search.DocumentIdentity;
import io.atherios.atherioshr.proposal.ProposalCategory;
import io.atherios.atherioshr.proposal.ProposalCategoryRepository;
import io.atherios.atherioshr.proposal.board.JsonProposalAttachment;
import io.atherios.atherioshr.proposal.board.ProposalListItem;
import io.atherios.atherioshr.proposal.proposalusers.attachments.ProposalAttachmentsDirectory;
import io.atherios.atherioshr.proposal.proposalusers.history.ProposalHistory;
import io.atherios.atherioshr.proposal.proposalusers.history.ProposalHistoryListItem;
import io.atherios.atherioshr.proposal.proposalusers.history.ProposalHistoryRepository;
import io.atherios.atherioshr.proposal.states.ProposalState;
import io.atherios.atherioshr.proposal.states.ProposalStatesRepository;
import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

@Service
@Transactional
@Validated
@AllArgsConstructor
public class ProposalService {

    private final ProposalCategoryRepository proposalCategoryRepository;
    private final ProposalRepository proposalRepository;
    private final UsersRepository usersRepository;
    private final ProposalStatesRepository proposalStatesRepository;
    private final FileDetailsRepository fileDetailsRepository;
    private final ProposalHistoryRepository proposalHistoryRepository;
    private final FileService fileService;
    private final ProposalAttachmentsDirectory proposalAttachmentsDirectory;
    private final ApplicationEventPublisher applicationEventPublisher;

    public ProposalListItem findOne(int proposalId) {
        Proposal proposal = proposalRepository.find(proposalId);
        return asListItem(proposal);
    }

    public void createProposal(int proposalCategoryId, @Valid JsonProposal jsonProposal) {
        ProposalCategory proposalCategory = proposalCategoryRepository.find(proposalCategoryId);
        ProposalState state = proposalStatesRepository.find(jsonProposal.getStateId());
        Set<FileDetails> files = fileDetailsRepository.getAllbyIds(jsonProposal.getFileIds());

        files.forEach(file -> fileService.moveFile(file.getId(), proposalAttachmentsDirectory));
        Proposal proposal = Proposal.builder()
                .category(proposalCategory)
                .files(files)
                .state(state)
                .createdDate(Instant.now())
                .user(loggedUser())
                .build();
        proposalRepository.save(proposal);
        applicationEventPublisher.publishEvent(new DocumentChangedEvent(new DocumentIdentity(Proposal.class, proposal.getId())));
    }

    public List<ProposalHistoryListItem> findHistory(int proposalId) {
        return proposalHistoryRepository.findByProposal(proposalId).stream()
                .sorted(Comparator.comparing(ProposalHistory::getTime).reversed())
                .map(ProposalHistoryListItem::new)
                .collect(Collectors.toList());
    }

    private User loggedUser() {
        AtheriosAuthentication user = (AtheriosAuthentication) SecurityContextHolder.getContext().getAuthentication();
        return usersRepository.find(user.getName());
    }

    public void switchState(int proposalId, @Valid JsonProposal jsonProposal) {
        Proposal proposal = proposalRepository.find(proposalId);
        ProposalState state = proposalStatesRepository.find(jsonProposal.getStateId());

        ProposalHistory historyEntry = ProposalHistory.builder()
                .previousState(proposal.getState())
                .nextState(state)
                .proposal(proposal)
                .time(Instant.now())
                .user(loggedUser())
                .build();
        proposalHistoryRepository.save(historyEntry);
        proposal.setState(state);
        applicationEventPublisher.publishEvent(new DocumentChangedEvent(new DocumentIdentity(Proposal.class, proposal.getId())));
    }

    public void resolveProposal(int proposalId) {
        Proposal proposal = proposalRepository.find(proposalId);
        proposalRepository.delete(proposal);
        applicationEventPublisher.publishEvent(new DocumentChangedEvent(new DocumentIdentity(Proposal.class, proposal.getId())));
    }


    public List<ProposalListItem> currentUserProposals() {
        String username = loggedUser().getUsername();
        return proposalRepository.findByUser(username).stream()
                .map(ProposalService::asListItem)
                .collect(Collectors.toList());
    }

    private static ProposalListItem asListItem(Proposal proposal) {
        Set<JsonProposalAttachment> attachments = proposal.getFiles().stream()
                .map(file -> new JsonProposalAttachment(file, "/api/proposal-attachments/" + file.getId() + "/file"))
                .collect(Collectors.toSet());

        return ProposalListItem.preconfigure(proposal)
                .files(attachments)
                .build();
    }
}

