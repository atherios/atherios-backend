package io.atherios.atherioshr.proposal.proposalusers.attachments;

import io.atherios.atherioshr.files.AtheriosDirectory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.nio.file.Path;

@Component
public class ProposalAttachmentsDirectory implements AtheriosDirectory {

    private final Path directoryPath;

    public ProposalAttachmentsDirectory(@Value("${proposals.attachments.directory-path}") Path directoryPath) {
        this.directoryPath = AtheriosDirectory.createDirectory(directoryPath);
    }

    @Override
    public Path path() {
        return directoryPath;
    }
}
