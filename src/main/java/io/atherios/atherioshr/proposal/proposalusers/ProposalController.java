package io.atherios.atherioshr.proposal.proposalusers;

import io.atherios.atherioshr.proposal.board.ProposalListItem;
import io.atherios.atherioshr.proposal.proposalusers.history.ProposalHistoryListItem;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/proposals")
public class ProposalController {

    private final ProposalService proposalService;

    @GetMapping("/{proposalId}")
    public ProposalListItem findOne(@PathVariable("proposalId") int proposalId) {
        return proposalService.findOne(proposalId);
    }

    @GetMapping("/{proposalId}/history")
    public List<ProposalHistoryListItem> findHistory(@PathVariable("proposalId") int proposalId) {
        return proposalService.findHistory(proposalId);
    }

    @PutMapping("/{proposalId}")
    public ResponseEntity<?> switchState(@PathVariable("proposalId") int proposalId,
                                         @RequestBody JsonProposal jsonProposal) {
        proposalService.switchState(proposalId, jsonProposal);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{proposalId}")
    public ResponseEntity<?> resolveProposal(@PathVariable("proposalId") int proposalId) {
        proposalService.resolveProposal(proposalId);
        return ResponseEntity.noContent().build();
    }

}
