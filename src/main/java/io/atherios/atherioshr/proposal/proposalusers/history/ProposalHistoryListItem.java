package io.atherios.atherioshr.proposal.proposalusers.history;

import io.atherios.atherioshr.auth.user.profile.JsonUserProfile;
import lombok.Getter;

import java.time.Instant;

@Getter
public class ProposalHistoryListItem {

    private final int previousStateId;
    private final int nextStateId;
    private final JsonUserProfile user;
    private final Instant actionTime;

    public ProposalHistoryListItem(ProposalHistory history) {
        this.previousStateId = history.getPreviousState().getId();
        this.nextStateId = history.getNextState().getId();
        this.user = new JsonUserProfile(history.getUser());
        this.actionTime = history.getTime();
    }


}
