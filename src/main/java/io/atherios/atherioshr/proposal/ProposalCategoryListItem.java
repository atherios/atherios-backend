package io.atherios.atherioshr.proposal;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ProposalCategoryListItem {

    private final int id;
    @JsonUnwrapped
    private final JsonProposalCategory category;
    private final int activeProposals;

    public static ProposalCategoryListItemBuilder preconfigure(ProposalCategory proposalCategory) {
        return ProposalCategoryListItem.builder()
                .id(proposalCategory.getId())
                .category(new JsonProposalCategory(proposalCategory));
    }
}
