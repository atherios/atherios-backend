package io.atherios.atherioshr.proposal;

import io.atherios.atherioshr.proposal.states.ProposalState;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "proposals_category")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class ProposalCategory {

    @Id
    @Column(name = "proposals_category_id")
    @GeneratedValue(generator = "proposals_category_gen")
    @SequenceGenerator(name = "proposals_category_gen", sequenceName = "proposals_category_seq")
    private Integer id;

    @Column(name = "summary", nullable = false, length = 250)
    private String summary;

    @OneToMany(mappedBy = "proposalCategory", orphanRemoval = true, cascade = CascadeType.ALL)
    @OrderColumn(name = "ordinal", nullable = false)
    private List<ProposalState> states;

    public List<ProposalState> getStates() {
        if (states == null) {
            states = new ArrayList<>();
        }
        return states;
    }

    public void setStates(List<ProposalState> states) {
        getStates().clear();
        getStates().addAll(states);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProposalCategory)) {
            return false;
        }
        ProposalCategory that = (ProposalCategory) o;
        return Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
