package io.atherios.atherioshr.proposal;

import java.util.List;

import io.atherios.atherioshr.proposal.proposalusers.JsonProposal;
import io.atherios.atherioshr.proposal.proposalusers.ProposalService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@RequestMapping("/proposal-categories")
public class ProposalCategoryController {

    private final ProposalCategoryService proposalCategoryService;
    private final ProposalService proposalService;

    @GetMapping
    public List<ProposalCategoryListItem> listAll() {
        return proposalCategoryService.listAll();
    }

    @PostMapping("/{proposalCategoryId}/proposals")
    public ResponseEntity<?> createProposal(@PathVariable("proposalCategoryId") int proposalCategoryId,
                                            @RequestBody JsonProposal jsonProposal) {
        proposalService.createProposal(proposalCategoryId, jsonProposal);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{proposalId}")
    public ProposalCategoryListItem findOne(@PathVariable("proposalId") int proposalId) {
        return proposalCategoryService.findOne(proposalId);
    }

    @PostMapping
    public void save(@RequestBody JsonProposalCategory jsonProposalCategory) {
        proposalCategoryService.create(jsonProposalCategory);
    }

    @PutMapping("/{id}")
    public void update(@PathVariable("id") int id,
                       @RequestBody JsonProposalCategory jsonProposalCategory) {
        proposalCategoryService.update(id, jsonProposalCategory);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") int id) {
        proposalCategoryService.delete(id);
    }
}
