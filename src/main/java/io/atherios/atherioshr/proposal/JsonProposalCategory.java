package io.atherios.atherioshr.proposal;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

@Getter
public class JsonProposalCategory {

    @Length(max = 250)
    @NotEmpty
    @NotNull
    private String summary;

    public JsonProposalCategory(ProposalCategory proposalCategory) {
        this.summary = proposalCategory.getSummary();

    }

    @JsonCreator
    public JsonProposalCategory(@JsonProperty("summary") String summary) {
        this.summary = summary;
    }
}
