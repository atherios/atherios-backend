package io.atherios.atherioshr.proposal.categoryattachments;

import io.atherios.atherioshr.files.FileDetails;
import io.atherios.atherioshr.files.FileDetailsRepository;
import io.atherios.atherioshr.files.FileService;
import io.atherios.atherioshr.lucene.DocumentChangedEvent;
import io.atherios.atherioshr.lucene.search.DocumentIdentity;
import io.atherios.atherioshr.proposal.ProposalCategory;
import io.atherios.atherioshr.proposal.ProposalCategoryRepository;
import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.transaction.Transactional;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
@Validated
@AllArgsConstructor
public class ProposalCategoryAttachmentService {

    private final ProposalCategoryRepository proposalCategoryRepository;
    private final ProposalCategoryAttachmentRepository proposalCategoryAttachmentRepository;
    private final FileDetailsRepository fileDetailsRepository;
    private final ProposalCategoryAttachmentDirectory proposalCategoryAttachmentDirectory;
    private final FileService fileService;
    private final ApplicationEventPublisher applicationEventPublisher;

    public List<ProposalCategoryAttachmentListItem> findByProposalCategory(int categoryId) {
        ProposalCategory category = proposalCategoryRepository.find(categoryId);
        return proposalCategoryAttachmentRepository.findByProposalCategory(category.getId()).stream()
                .map(categoryAttachment -> ProposalCategoryAttachmentListItem.preconfigureWith(categoryAttachment)
                        .link("/proposal-categories/" + categoryId + "/attachments/" + categoryAttachment.getId() + "/file")
                        .build())
                .collect(Collectors.toList());
    }

    public void saveProposalCategoryAttachment(int proposalCategoryId, JsonProposalCategoryAttachment attachment) {
        ProposalCategory category = proposalCategoryRepository.find(proposalCategoryId);
        FileDetails fileDetails = fileDetailsRepository.find(attachment.getFileId());

        ProposalCategoryAttachment categoryAttachment = ProposalCategoryAttachment.builder()
                .description(attachment.getDescription())
                .fileDetails(fileDetails)
                .proposalCategory(category)
                .build();

        proposalCategoryAttachmentRepository.save(categoryAttachment);
        fileService.moveFile(attachment.getFileId(), proposalCategoryAttachmentDirectory);
        applicationEventPublisher.publishEvent(new DocumentChangedEvent(new DocumentIdentity(ProposalCategoryAttachment.class, categoryAttachment.getId())));
    }

    public void deleteProposalCategoryAttachment(int attachmentId) {
        ProposalCategoryAttachment attachment = proposalCategoryAttachmentRepository.find(attachmentId);
        Path attachmentFile = proposalCategoryAttachmentDirectory.findFile(attachment.getFileDetails())
                .orElseThrow(ProposalCategoryAttachmentService::attachmentFileNotFound);

        deleteAttachmentFile(attachmentFile);
        proposalCategoryAttachmentRepository.delete(attachment);
        applicationEventPublisher.publishEvent(new DocumentChangedEvent(new DocumentIdentity(ProposalCategoryAttachment.class, attachment.getId())));
    }

    private static RuntimeException attachmentFileNotFound() {
        return new RuntimeException("proposal-categories.attachments.file-not-found");
    }

    private static void deleteAttachmentFile(Path attachmentFile) {
        try {
            Files.deleteIfExists(attachmentFile);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    public Optional<AtheriosFile> findAttachment(int attachmentId) {
        ProposalCategoryAttachment attachment = proposalCategoryAttachmentRepository.find(attachmentId);
        return proposalCategoryAttachmentDirectory.findFile(attachment.getFileDetails())
                .map(path -> new AtheriosFile(path, attachment.getFileDetails().getMime()));
    }
}
