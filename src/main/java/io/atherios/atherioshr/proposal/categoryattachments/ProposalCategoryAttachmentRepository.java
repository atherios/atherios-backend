package io.atherios.atherioshr.proposal.categoryattachments;

import io.atherios.atherioshr.persistence.AtheriosRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProposalCategoryAttachmentRepository extends AtheriosRepository<ProposalCategoryAttachment, Integer> {

    @Query("select a from ProposalCategoryAttachment a where a.proposalCategory.id = :categoryId")
    List<ProposalCategoryAttachment> findByProposalCategory(@Param("categoryId") int categoryId);
}
