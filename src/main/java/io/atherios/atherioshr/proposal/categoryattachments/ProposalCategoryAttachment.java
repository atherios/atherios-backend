package io.atherios.atherioshr.proposal.categoryattachments;

import io.atherios.atherioshr.files.FileDetails;
import io.atherios.atherioshr.proposal.ProposalCategory;
import lombok.*;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "proposal_category_attachment")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class ProposalCategoryAttachment {

    @Id
    @Column(name = "attachment_id")
    @GeneratedValue(generator = "attachment_id_gen")
    @SequenceGenerator(name = "attachment_id_gen", sequenceName = "proposal_category_attachment_seq")
    private Integer id;

    @Column(name = "description")
    private String description;

    @OneToOne(optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "file_id")
    private FileDetails fileDetails;

    @ManyToOne(optional = false)
    @JoinColumn(name = "category_id")
    private ProposalCategory proposalCategory;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProposalCategoryAttachment)) return false;
        ProposalCategoryAttachment that = (ProposalCategoryAttachment) o;
        return Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
