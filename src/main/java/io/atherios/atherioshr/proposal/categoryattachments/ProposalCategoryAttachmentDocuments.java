package io.atherios.atherioshr.proposal.categoryattachments;

import com.google.common.primitives.Ints;
import io.atherios.atherioshr.files.FileDetails;
import io.atherios.atherioshr.lucene.AtheriosDocument;
import io.atherios.atherioshr.lucene.AtheriosDocuments;
import io.atherios.atherioshr.lucene.LuceneDocumentBuilder;
import io.atherios.atherioshr.lucene.index.IndexFields;
import lombok.AllArgsConstructor;
import org.apache.tika.Tika;
import org.apache.tika.exception.TikaException;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Optional;
import java.util.stream.Stream;

@Component
@AllArgsConstructor
public class ProposalCategoryAttachmentDocuments implements AtheriosDocuments {

    private final ProposalCategoryAttachmentRepository proposalCategoryAttachmentRepository;
    private final ProposalCategoryAttachmentDirectory proposalCategoryAttachmentDirectory;
    private final Tika tika;

    @Override
    public Stream<AtheriosDocument> allDocuments() {
        return proposalCategoryAttachmentRepository.findAll().stream()
                .map(this::asDocument);
    }

    private AtheriosDocument asDocument(ProposalCategoryAttachment attachment) {
        FileDetails fileDetails = attachment.getFileDetails();
        return LuceneDocumentBuilder.builder(ProposalCategoryAttachment.class, attachment.getId())
                .withField(IndexFields.TEXT_SEARCH, fileDetails.getFileName())
                .withField(IndexFields.STRINGS_SEARCH, fileDetails.getAuthor())
                .withField(IndexFields.TEXT_SEARCH, extractText(fileDetails))
                .build();
    }

    private String extractText(FileDetails details) {
        return proposalCategoryAttachmentDirectory.findFile(details)
                .map(file -> {
                    try {
                        return tika.parseToString(file);
                    } catch (IOException | TikaException e) {
                        return "";
                    }
                })
                .orElse("");
    }

    @Override
    public Optional<AtheriosDocument> findIndexDocument(String entityId) {
        return proposalCategoryAttachmentRepository.tryFind(Integer.parseInt(entityId)).map(this::asDocument);
    }

    @Override
    public Optional<Object> findSearchHit(String id) {
        return proposalCategoryAttachmentRepository.tryFind(Ints.tryParse(id))
                .map(attachment -> ProposalCategoryAttachmentListItem.preconfigureWith(attachment).build());
    }

    @Override
    public String documentType() {
        return ProposalCategoryAttachment.class.getCanonicalName();
    }
}
