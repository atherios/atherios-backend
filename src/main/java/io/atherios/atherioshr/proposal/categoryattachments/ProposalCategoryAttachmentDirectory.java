package io.atherios.atherioshr.proposal.categoryattachments;

import io.atherios.atherioshr.files.AtheriosDirectory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.nio.file.Path;

@Component
public class ProposalCategoryAttachmentDirectory implements AtheriosDirectory {

    private final Path proposalCategoryAttachmentsDirectory;

    public ProposalCategoryAttachmentDirectory(@Value("${proposal-categories.attachments.directory-path}") Path proposalCategoryAttachmentsDirectory) {
        this.proposalCategoryAttachmentsDirectory = AtheriosDirectory.createDirectory(proposalCategoryAttachmentsDirectory);
    }

    @Override
    public Path path() {
        return proposalCategoryAttachmentsDirectory;
    }
}
