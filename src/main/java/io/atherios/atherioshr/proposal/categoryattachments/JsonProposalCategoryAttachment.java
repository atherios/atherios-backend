package io.atherios.atherioshr.proposal.categoryattachments;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class JsonProposalCategoryAttachment {

    private final String description;
    private final int fileId;

    public JsonProposalCategoryAttachment(ProposalCategoryAttachment attachment) {
        this.description = attachment.getDescription();
        this.fileId = attachment.getFileDetails().getId();
    }

    @JsonCreator
    public JsonProposalCategoryAttachment(@JsonProperty("description") String description,
                                          @JsonProperty("file_id") int fileId) {
        this.description = description;
        this.fileId = fileId;
    }
}
