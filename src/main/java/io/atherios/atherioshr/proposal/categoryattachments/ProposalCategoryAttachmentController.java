package io.atherios.atherioshr.proposal.categoryattachments;

import lombok.AllArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
public class ProposalCategoryAttachmentController {

    private final ProposalCategoryAttachmentService proposalCategoryAttachmentService;

    @GetMapping("/proposal-categories/{categoryId}/attachments")
    public List<ProposalCategoryAttachmentListItem> findByCategoryId(@PathVariable("categoryId") int categoryId) {
        return proposalCategoryAttachmentService.findByProposalCategory(categoryId);
    }

    @GetMapping("/proposal-categories/{categoryId}/attachments/{attachmentId}/file")
    public ResponseEntity<Resource> attachment(@PathVariable("attachmentId") int attachmentId) {
        return proposalCategoryAttachmentService.findAttachment(attachmentId)
                .map(atheriosFile -> ResponseEntity.ok()
                        .header(HttpHeaders.CONTENT_TYPE, atheriosFile.getMime())
                        .body(atheriosFile.getResource()))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping("/proposal-categories/{categoryId}/attachments")
    public void saveCategoryAttachment(@PathVariable("categoryId") int categoryId,
                                       @RequestBody JsonProposalCategoryAttachment attachment) {
        proposalCategoryAttachmentService.saveProposalCategoryAttachment(categoryId, attachment);
    }

    @DeleteMapping("/proposal-category-attachments/{attachmentId}")
    public void saveCategoryAttachment(@PathVariable("attachmentId") int attachmentId) {
        proposalCategoryAttachmentService.deleteProposalCategoryAttachment(attachmentId);
    }


}
