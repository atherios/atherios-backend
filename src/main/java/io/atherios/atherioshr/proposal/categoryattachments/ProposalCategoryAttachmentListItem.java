package io.atherios.atherioshr.proposal.categoryattachments;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import io.atherios.atherioshr.files.JsonFileDetails;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ProposalCategoryAttachmentListItem {

    private final int id;
    @JsonUnwrapped
    private final JsonProposalCategoryAttachment attachment;
    private final String link;
    private final int categoryId;
    @JsonUnwrapped
    private final JsonFileDetails fileDetails;

    public static ProposalCategoryAttachmentListItemBuilder preconfigureWith(ProposalCategoryAttachment attachment) {
        return ProposalCategoryAttachmentListItem.builder()
                .id(attachment.getId())
                .categoryId(attachment.getProposalCategory().getId())
                .attachment( new JsonProposalCategoryAttachment(attachment))
                .fileDetails(new JsonFileDetails(attachment.getFileDetails()));
    }

}
