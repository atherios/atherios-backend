package io.atherios.atherioshr.proposal.categoryattachments;

import lombok.Getter;
import org.springframework.core.io.PathResource;
import org.springframework.core.io.Resource;

import java.nio.file.Path;

@Getter
public class AtheriosFile {

    private final Resource resource;
    private final String mime;

    public AtheriosFile(Path file, String mime) {
        this.resource = new PathResource(file);
        this.mime = mime;
    }
}
