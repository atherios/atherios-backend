package io.atherios.atherioshr.proposal.states;

import io.atherios.atherioshr.persistence.AtheriosRepository;

public interface ProposalStatesRepository extends AtheriosRepository<ProposalState, Integer> {
}
