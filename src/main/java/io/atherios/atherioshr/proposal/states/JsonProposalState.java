package io.atherios.atherioshr.proposal.states;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import org.hibernate.validator.constraints.NotBlank;

@Getter
public class JsonProposalState {

    @NotNull
    @NotBlank
    private final String label;
    private final boolean closed;

    public JsonProposalState(ProposalState state) {
        this.label = state.getLabel();
        this.closed = state.isClosed();
    }

    @JsonCreator
    public JsonProposalState(@JsonProperty("label") String label,
                             @JsonProperty("closed") boolean closed) {
        this.label = label;
        this.closed = closed;
    }
}
