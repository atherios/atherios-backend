package io.atherios.atherioshr.proposal.states;

import com.google.common.primitives.Ints;
import io.atherios.atherioshr.lucene.AtheriosDocument;
import io.atherios.atherioshr.lucene.AtheriosDocuments;
import io.atherios.atherioshr.lucene.LuceneDocumentBuilder;
import io.atherios.atherioshr.lucene.index.IndexFields;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.stream.Stream;

@Component
@AllArgsConstructor
public class ProposalStatesDocuments implements AtheriosDocuments {

    private final ProposalStatesRepository proposalStatesRepository;

    @Override
    public Stream<AtheriosDocument> allDocuments() {
        return proposalStatesRepository.findAll().stream()
                .map(this::asDocument);
    }

    @Override
    public Optional<Object> findSearchHit(String id) {
        return proposalStatesRepository.tryFind(Ints.tryParse(id))
                .map(ProposalStateListItem::new);
    }

    @Override
    public String documentType() {
        return ProposalState.class.getCanonicalName();
    }

    @Override
    public Optional<AtheriosDocument> findIndexDocument(String entityId) {
        return proposalStatesRepository.tryFind(Integer.parseInt(entityId)).map(this::asDocument);
    }

    private AtheriosDocument asDocument(ProposalState proposalState) {
        return LuceneDocumentBuilder.builder(ProposalState.class, proposalState.getId())
                .withField(IndexFields.STRINGS_SEARCH, '#' + proposalState.getId().toString())
                .withField(IndexFields.STRINGS_SEARCH, proposalState.getLabel())
                .build();
    }
}
