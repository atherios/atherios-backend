package io.atherios.atherioshr.proposal.states;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProposalStateController {

    private final ProposalStateService proposalStateService;

    public ProposalStateController(ProposalStateService proposalStateService) {
        this.proposalStateService = proposalStateService;
    }

    @GetMapping("/proposal-categories/{proposalCategoryId}/states")
    public List<ProposalStateListItem> listStates(@PathVariable("proposalCategoryId") int proposalCategoryId) {
        return proposalStateService.listStates(proposalCategoryId);
    }

    @PutMapping("/proposal-categories/{proposalCategoryId}/states")
    public ResponseEntity<?> saveStatesOrder(@PathVariable("proposalCategoryId") int proposalCategoryId,
                                             @RequestBody List<Integer> stateIds) {
        proposalStateService.saveStatesOrder(proposalCategoryId, stateIds);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/proposal-categories/{proposalCategoryId}/states")
    public ResponseEntity<?> saveState(@PathVariable("proposalCategoryId") int proposalCategoryId,
                                       @RequestBody JsonProposalState state) {
        proposalStateService.save(proposalCategoryId, state);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/proposal-states/{stateId}")
    public ResponseEntity<?> updateState(@PathVariable("stateId") int stateId,
                                         @RequestBody JsonProposalState state) {
        proposalStateService.update(stateId, state);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/proposal-states/{stateId}")
    public ResponseEntity<?> delete(@PathVariable("stateId") int stateId) {
        proposalStateService.delete(stateId);
        return ResponseEntity.ok().build();
    }
}
