package io.atherios.atherioshr.proposal.states;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import lombok.Getter;

@Getter
public class ProposalStateListItem {

    private final int id;
    @JsonUnwrapped
    private final JsonProposalState state;
    private final int ordinal;
    private final int categoryId;

    public ProposalStateListItem(ProposalState state) {
        this.id = state.getId();
        this.ordinal = state.getOrdinal();
        this.state = new JsonProposalState(state);
        this.categoryId = state.getProposalCategory().getId();
    }
}
