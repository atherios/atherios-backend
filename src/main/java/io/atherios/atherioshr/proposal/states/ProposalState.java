package io.atherios.atherioshr.proposal.states;

import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import io.atherios.atherioshr.proposal.ProposalCategory;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "proposal_states")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class ProposalState {

    @Id
    @Column(name = "proposal_state_id")
    @GeneratedValue(generator = "proposal_states_gen", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "proposal_states_gen", sequenceName = "proposal_states_seq")
    private Integer id;

    @Column(name = "state_label", nullable = false)
    private String label;

    @Column(name = "closed", nullable = false)
    private boolean closed;

    @Column(name = "ordinal", nullable = false)
    private int ordinal;

    @ManyToOne(optional = false)
    @JoinColumn(name = "proposal_category_id")
    private ProposalCategory proposalCategory;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProposalState)) return false;
        ProposalState that = (ProposalState) o;
        return Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
