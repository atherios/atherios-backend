package io.atherios.atherioshr.proposal.states;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import javax.validation.Valid;

import io.atherios.atherioshr.lucene.DocumentChangedEvent;
import io.atherios.atherioshr.lucene.search.DocumentIdentity;
import io.atherios.atherioshr.proposal.ProposalCategory;
import io.atherios.atherioshr.proposal.ProposalCategoryRepository;
import io.atherios.atherioshr.proposal.proposalusers.Proposal;
import io.atherios.atherioshr.proposal.proposalusers.ProposalRepository;
import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

@Service
@Validated
@Transactional
@AllArgsConstructor
public class ProposalStateService {

    private final ProposalCategoryRepository proposalCategoryRepository;
    private final ProposalStatesRepository proposalStatesRepository;
    private final ProposalRepository proposalRepository;
    private final ApplicationEventPublisher applicationEventPublisher;

    public List<ProposalStateListItem> listStates(int proposalCategoryId) {
        ProposalCategory proposalCategory = proposalCategoryRepository.find(proposalCategoryId);

        return proposalCategory.getStates()
                               .stream()
                               .filter(Objects::nonNull)
                               .sorted(Comparator.comparingInt(ProposalState::getOrdinal))
                               .map(ProposalStateListItem::new)
                               .collect(Collectors.toList());
    }

    public void save(int proposalCategoryId, @Valid JsonProposalState jsonState) {
        ProposalCategory proposalCategory = proposalCategoryRepository.find(proposalCategoryId);

        ProposalState state = ProposalState.builder()
                                           .proposalCategory(proposalCategory)
                                           .label(jsonState.getLabel())
                                           .closed(jsonState.isClosed())
                                           .build();
        proposalCategory.getStates().add(state);
        proposalStatesRepository.flush();
        applicationEventPublisher.publishEvent(new DocumentChangedEvent(new DocumentIdentity(ProposalState.class, state.getId())));
    }

    public void update(int stateId, @Valid JsonProposalState jsonState) {
        ProposalState state = proposalStatesRepository.find(stateId);

        state.setClosed(jsonState.isClosed());
        state.setLabel(jsonState.getLabel());
        applicationEventPublisher.publishEvent(new DocumentChangedEvent(new DocumentIdentity(ProposalState.class, state.getId())));
    }

    public void saveStatesOrder(int proposalCategoryId, List<Integer> stateIds) {
        ProposalCategory proposalCategory = proposalCategoryRepository.find(proposalCategoryId);

        List<ProposalState> states = stateIds.stream()
                                             .map(proposalStatesRepository::find)
                                             .collect(Collectors.toList());
        proposalCategory.setStates(states);
    }

    public void delete(int stateId) {
        List<Proposal> proposals = proposalRepository.findByState(stateId);
        proposalRepository.delete(proposals);
        proposalStatesRepository.delete(stateId);
        applicationEventPublisher.publishEvent(new DocumentChangedEvent(new DocumentIdentity(ProposalState.class, stateId)));
    }
}
