package io.atherios.atherioshr.lucene.index;

import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexableField;
import org.apache.lucene.index.Term;

public enum IndexFields {
    DOCUMENT_ID {
        @Override
        public IndexableField field(String value) {
            return new StringField(fieldname(), value, Field.Store.YES);
        }
    },
    ENTITY_ID {
        @Override
        public IndexableField field(String value) {
            return new StringField(fieldname(), value, Field.Store.YES);
        }
    },

    DOCUMENT_TYPE {
        @Override
        public IndexableField field(String value) {
            return new StringField(fieldname(), value, Field.Store.YES);
        }
    },
    TEXT_SEARCH {
        @Override
        public IndexableField field(String value) {
            return new TextField(fieldname(), value, Field.Store.NO);
        }
    },
    STRINGS_SEARCH {
        @Override
        public IndexableField field(String value) {
            return new StringField(fieldname(), value, Field.Store.NO);
        }
    },
    MINOR_TEXT_SEARCH {
        @Override
        public IndexableField field(String value) {
            return new TextField(fieldname(), value, Field.Store.NO);
        }
    },
    MINOR_STRINGS_SEARCH {
        @Override
        public IndexableField field(String value) {
            return new StringField(fieldname(), value, Field.Store.NO);
        }
    };

    public abstract IndexableField field(String value);

    public String fieldname() {
        return name().toLowerCase();
    }


    public Term term(String value) {
        return new Term(fieldname(), value);
    }

}
