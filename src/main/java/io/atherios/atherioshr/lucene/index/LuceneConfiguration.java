package io.atherios.atherioshr.lucene.index;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.search.SearcherManager;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.tika.Tika;
import org.apache.tika.detect.TextDetector;
import org.apache.tika.parser.AutoDetectParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.nio.file.Path;

@Configuration
public class LuceneConfiguration {

    @Autowired
    @Value("${lucene.index-directory}")
    private Path indexDirectory;

    @Bean
    public Directory directory() throws IOException {
        return FSDirectory.open(indexDirectory);
    }


    @Bean
    public Tika tika() {
        return new Tika(new TextDetector(), new AutoDetectParser());
    }

    @Bean(destroyMethod = "close")
    public IndexWriter indexWriter(Directory directory) throws IOException {
        return new IndexWriter(directory, new IndexWriterConfig(new StandardAnalyzer()));
    }

    @Bean
    public SearcherManager searcherManager(IndexWriter indexWriter) throws IOException {
        return new SearcherManager(indexWriter, null);
    }

}
