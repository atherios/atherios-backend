package io.atherios.atherioshr.lucene.index;

import io.atherios.atherioshr.lucene.AtheriosDocument;
import io.atherios.atherioshr.lucene.AtheriosDocuments;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.lucene.index.IndexWriter;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

@Slf4j
@Component
@AllArgsConstructor
public class Indexer {

    private final List<AtheriosDocuments> atheriosDocuments;
    private final IndexWriter indexWriter;

    @EventListener
    public void onApplicationEvent(ContextRefreshedEvent event) throws IOException {
        indexAllDocuments();
    }


    private void indexAllDocuments() throws IOException {
        atheriosDocuments.parallelStream()
                .peek(atheriosDocuments -> log.info("Started indexing documents of type: {}", atheriosDocuments.documentType()))
                .flatMap(AtheriosDocuments::allDocuments)
                .peek(new ThresholdCommit(indexWriter, 100))
                .forEach(this::indexDocument);
        indexWriter.commit();
        log.info("Performing index commit.");
    }

    private void indexDocument(AtheriosDocument document) {
        try {
            indexWriter.updateDocument(document.identityTerm(), document);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    @PreDestroy
    private void closeIndex() throws IOException {
        log.info("Closing index.");
        indexWriter.close();
    }

    @Slf4j
    private static class ThresholdCommit implements Consumer<AtheriosDocument> {

        private final IndexWriter indexWriter;
        private final int threshold;
        private final AtomicInteger count = new AtomicInteger();

        public ThresholdCommit(IndexWriter indexWriter, int threshold) {
            this.indexWriter = indexWriter;
            this.threshold = threshold;
        }

        @Override
        public void accept(AtheriosDocument doc) {
            if (count.getAndIncrement() % threshold == 0) {
                try {
                    indexWriter.commit();
                    log.info("Performing index commit.");
                } catch (IOException e) {
                    throw new UncheckedIOException(e);
                }
            }
        }
    }
}
