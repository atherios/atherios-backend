package io.atherios.atherioshr.lucene;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.collect.Streams;

import java.util.List;
import java.util.stream.Stream;

public class Emails {

    private static final Splitter EMAIL_SPLITTER = Splitter.on(CharMatcher.anyOf("@"))
            .omitEmptyStrings()
            .trimResults();

    private static final Splitter USERNAME_SPLITTER = Splitter.on(CharMatcher.anyOf(".-"))
            .omitEmptyStrings()
            .trimResults();

    private static final Splitter DOMAIN_SPLITTER = Splitter.on(CharMatcher.anyOf("."))
            .omitEmptyStrings()
            .trimResults();

    public static Stream<String> parts(String email) {
        List<String> emailParts = EMAIL_SPLITTER.splitToList(email);

        String username = emailParts.get(0);
        String domain = emailParts.get(1);
        return Streams.concat(
                Stream.of(email),
                Stream.of(username),
                Stream.of(domain),
                Streams.stream(USERNAME_SPLITTER.split(domain)),
                Streams.stream(DOMAIN_SPLITTER.split(domain))
        );
    }

}
