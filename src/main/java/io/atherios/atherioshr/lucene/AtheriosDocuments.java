package io.atherios.atherioshr.lucene;

import java.util.Optional;
import java.util.stream.Stream;

public interface AtheriosDocuments {

    Stream<AtheriosDocument> allDocuments();

    Optional<Object> findSearchHit(String id);

    String documentType();

    Optional<AtheriosDocument> findIndexDocument(String entityId);
}
