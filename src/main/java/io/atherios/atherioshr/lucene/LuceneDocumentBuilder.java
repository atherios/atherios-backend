package io.atherios.atherioshr.lucene;

import com.google.common.collect.ImmutableList;
import io.atherios.atherioshr.lucene.index.IndexFields;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexableField;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class LuceneDocumentBuilder {

    private final ImmutableList.Builder<IndexableField> fields = ImmutableList.builder();
    private final Class<?> clazz;
    private final Serializable entityId;

    public static LuceneDocumentBuilder builder(Class<?> clazz, int entityId) {
        return new LuceneDocumentBuilder(clazz, entityId);
    }

    public static LuceneDocumentBuilder builder(Class<?> clazz, String entityId) {
        return new LuceneDocumentBuilder(clazz, entityId);
    }

    public LuceneDocumentBuilder withField(IndexFields indexFields, String value) {
        IndexableField indexableField = indexFields.field(value);
        fields.add(indexableField);
        return this;
    }

    private LuceneDocumentBuilder(Class<?> clazz, Serializable entityId) {
        this.clazz = clazz;
        this.entityId = entityId;
    }

    public LuceneDocumentBuilder withEmailField(IndexFields field, String email) {
        Emails.parts(email)
                .map(field::field)
                .forEach(fields::add);
        return this;
    }

    public AtheriosDocument build() {
        List<IndexableField> fields = this.fields.build();

        Document document = new Document();
        document.add(IndexFields.DOCUMENT_ID.field(AtheriosDocument.documentId(clazz, entityId)));
        document.add(IndexFields.DOCUMENT_TYPE.field(documentType()));
        document.add(IndexFields.ENTITY_ID.field(entityId.toString()));
        fields.forEach(document::add);

        return new AtheriosDocument(entityId.toString(), documentType(), document);
    }

    private String documentType() {
        return Objects.requireNonNull(clazz.getCanonicalName());
    }
}
