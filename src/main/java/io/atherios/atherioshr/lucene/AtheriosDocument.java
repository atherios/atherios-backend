package io.atherios.atherioshr.lucene;

import io.atherios.atherioshr.lucene.index.IndexFields;
import lombok.Getter;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexableField;
import org.apache.lucene.index.Term;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;

@Getter
public class AtheriosDocument implements Iterable<IndexableField> {

    private final String entityId;
    private final String documentType;
    private final Document document;

    public static String documentId(Class<?> clazz, Serializable entityId) {
        return documentId(clazz.getCanonicalName(), entityId);
    }

    public static String documentId(String clazz, Serializable entityId) {
        return clazz + '-' + entityId;
    }

    AtheriosDocument(String entityId, String documentType, Document document) {
        this.entityId = entityId;
        this.documentType = documentType;
        this.document = document;
    }

    public Term identityTerm() {
        return IndexFields.DOCUMENT_ID.term(documentId(documentType, entityId));
    }

    @Override
    public Iterator<IndexableField> iterator() {
        return document.iterator();
    }

    @Override
    public Spliterator<IndexableField> spliterator() {
        return document.spliterator();
    }

    @Override
    public void forEach(Consumer<? super IndexableField> action) {
        document.forEach(action);
    }
}
