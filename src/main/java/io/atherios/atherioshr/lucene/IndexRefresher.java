package io.atherios.atherioshr.lucene;

import io.atherios.atherioshr.lucene.search.AtheriosDocumentProviders;
import io.atherios.atherioshr.lucene.search.DocumentIdentity;
import lombok.AllArgsConstructor;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.search.SearcherManager;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionPhase;
import org.springframework.transaction.event.TransactionalEventListener;

import java.io.IOException;
import java.util.Optional;

@Component
@AllArgsConstructor
class IndexRefresher {

    private final AtheriosDocumentProviders providers;
    private final IndexWriter indexWriter;
    private final SearcherManager searcherManager;

    @TransactionalEventListener(phase = TransactionPhase.AFTER_COMPLETION)
    public void documentChanged(DocumentChangedEvent event) throws IOException {
        for (DocumentIdentity documentIdentity : event.getIdentities())
            synchronizeIndex(documentIdentity);
        indexWriter.commit();
        searcherManager.maybeRefresh();
    }

    private void synchronizeIndex(DocumentIdentity documentIdentity) throws IOException {
        Optional<AtheriosDocument> indexDocument = providers.findIndexDocument(documentIdentity);

        if (!indexDocument.isPresent()) {
            indexWriter.deleteDocuments(documentIdentity.asTerm());
        } else {
            AtheriosDocument document = indexDocument.orElseThrow(() -> new IllegalStateException("Impossible."));
            indexWriter.updateDocument(documentIdentity.asTerm(), document);
        }
    }

}
