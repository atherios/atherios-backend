package io.atherios.atherioshr.lucene.search;

import com.google.common.base.CaseFormat;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Streams;
import com.google.common.io.Closer;
import io.atherios.atherioshr.lucene.Queries;
import io.atherios.atherioshr.lucene.index.IndexFields;
import lombok.AllArgsConstructor;
import org.apache.lucene.document.Document;
import org.apache.lucene.search.*;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@AllArgsConstructor
public class Searcher {

    private static final int RESULTS_LIMIT = 40;
    private final SearcherManager searcherManager;
    private final AtheriosDocumentProviders documentProviders;

    public List<AtheriosSearchHit> search(String phrase) {
        try (Closer closer = Closer.create()) {
            IndexSearcher searcher = searcherManager.acquire();
            closer.register(() -> searcherManager.release(searcher));

            return performSearch(searcher, phrase)
                    .map(documentIdentity ->
                            documentProviders.findSearchHit(documentIdentity)
                                    .map(document -> new AtheriosSearchHit(hyphenize(documentIdentity), document)))
                    .flatMap(Streams::stream)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            return ImmutableList.of();
        }
    }

    private static String hyphenize(DocumentIdentity documentIdentity) {
        String className = Iterables.getLast(Splitter.on('.').split(documentIdentity.getDocumentType()));
        return CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_HYPHEN, className);
    }

    private Stream<DocumentIdentity> performSearch(IndexSearcher searcher, String phrase) throws IOException {
        Query query = Queries.query(phrase);
        TopDocs topDocs = searcher.search(query, RESULTS_LIMIT);

        Stream.Builder<DocumentIdentity> builder = Stream.builder();
        for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
            Document document = searcher.doc(scoreDoc.doc);

            String entityId = document.get(IndexFields.ENTITY_ID.fieldname());
            String documentType = document.get(IndexFields.DOCUMENT_TYPE.fieldname());

            DocumentIdentity identity = new DocumentIdentity(documentType, entityId);
            builder.add(identity);
        }
        return builder.build();
    }

}
