package io.atherios.atherioshr.lucene.search;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
public class SearchController {

    private final Searcher searcher;

    @GetMapping("/search")
    public List<AtheriosSearchHit> search(@RequestParam("q") String phrase) {
        return searcher.search(phrase);
    }

}
