package io.atherios.atherioshr.lucene.search;

import io.atherios.atherioshr.lucene.AtheriosDocument;
import io.atherios.atherioshr.lucene.index.IndexFields;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.lucene.index.Term;

import java.io.Serializable;
import java.util.Objects;

@Getter
@AllArgsConstructor
public class DocumentIdentity {

    private final String documentType;
    private final String entityId;

    public DocumentIdentity(Class<?> clazz, Serializable entityId) {
        this.documentType = clazz.getCanonicalName();
        this.entityId = entityId.toString();
    }

    public Term asTerm() {
        return IndexFields.DOCUMENT_ID.term(AtheriosDocument.documentId(documentType, entityId));
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DocumentIdentity)) return false;
        DocumentIdentity that = (DocumentIdentity) o;
        return Objects.equals(getDocumentType(), that.getDocumentType()) &&
                Objects.equals(getEntityId(), that.getEntityId());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getDocumentType(), getEntityId());
    }
}
