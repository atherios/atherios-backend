package io.atherios.atherioshr.lucene.search;

import com.google.common.collect.Maps;
import io.atherios.atherioshr.lucene.AtheriosDocument;
import io.atherios.atherioshr.lucene.AtheriosDocuments;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Component
public class AtheriosDocumentProviders {

    private final Map<String, AtheriosDocuments> providers;

    public AtheriosDocumentProviders(List<AtheriosDocuments> providers) {
        this.providers = Maps.uniqueIndex(providers, AtheriosDocuments::documentType);
    }

    public Optional<?> findSearchHit(DocumentIdentity documentIdentity) {
        AtheriosDocuments provider = providers.get(documentIdentity.getDocumentType());

        if (provider == null)
            return Optional.empty();

        return provider.findSearchHit(documentIdentity.getEntityId());
    }

    public Optional<AtheriosDocument> findIndexDocument(DocumentIdentity documentIdentity) {
        AtheriosDocuments provider = providers.get(documentIdentity.getDocumentType());

        if (provider == null)
            return Optional.empty();

        return provider.findIndexDocument(documentIdentity.getEntityId());
    }
}
