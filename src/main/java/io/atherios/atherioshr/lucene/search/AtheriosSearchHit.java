package io.atherios.atherioshr.lucene.search;

import lombok.Getter;

@Getter
public class AtheriosSearchHit {

    private final String documentType;
    private final Object payload;

    public AtheriosSearchHit(String documentType, Object payload) {
        this.documentType = documentType;
        this.payload = payload;
    }
}
