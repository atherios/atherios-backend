package io.atherios.atherioshr.lucene;

import com.google.common.collect.ImmutableList;
import io.atherios.atherioshr.lucene.search.DocumentIdentity;
import lombok.Getter;

import java.util.List;

/**
 * Event publikowany w momencie zmiany/usunięcia/pojawienia się dokumentu w bazie.
 */
@Getter
public class DocumentChangedEvent {

    private final List<DocumentIdentity> identities;

    public DocumentChangedEvent(DocumentIdentity identities) {
        this.identities = ImmutableList.of(identities);
    }

    public DocumentChangedEvent(List<DocumentIdentity> identities){
        this.identities = identities;
    }
}
