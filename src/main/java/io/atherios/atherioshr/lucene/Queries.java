package io.atherios.atherioshr.lucene;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.collect.Streams;
import io.atherios.atherioshr.lucene.index.IndexFields;
import org.apache.lucene.search.*;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Queries {

    private static final float MINOR_FIELDS_DOWNBOOST = .8f;
    private static final float IMPORTANT_FIELDS_BOOST = 4f;
    private static final int FUZZY_QUERY_THRESHOLD = 2;

    private static final Splitter WORDS_SPLITTER = Splitter.on(CharMatcher.whitespace())
            .trimResults()
            .omitEmptyStrings();

    public static Query query(String phrase) {
        List<String> words = Streams.stream(WORDS_SPLITTER.split(phrase))
                .filter(word -> word.length() > 2)
                .collect(Collectors.toList());

        BooleanQuery.Builder queryBuilder = new BooleanQuery.Builder();
        queryBuilder.setMinimumNumberShouldMatch(1);
        words.stream()
                .flatMap(Queries::clauses)
                .forEach(queryBuilder::add);

        return queryBuilder.build();
    }

    private static Stream<BooleanClause> clauses(String word) {
        Stream.Builder<Query> builder = Stream.builder();

        builder.add(new BoostQuery(new WildcardQuery(IndexFields.TEXT_SEARCH.term(word + '*')), IMPORTANT_FIELDS_BOOST));
        builder.add(new BoostQuery(new WildcardQuery(IndexFields.STRINGS_SEARCH.term(word + '*')), IMPORTANT_FIELDS_BOOST));
        builder.add(new WildcardQuery(IndexFields.MINOR_TEXT_SEARCH.term(word + '*')));
        builder.add(new WildcardQuery(IndexFields.MINOR_STRINGS_SEARCH.term(word + '*')));

        if (word.length() > FUZZY_QUERY_THRESHOLD) {
            builder.add(new FuzzyQuery(IndexFields.TEXT_SEARCH.term(word)));
            builder.add(new FuzzyQuery(IndexFields.STRINGS_SEARCH.term(word)));
            builder.add(new BoostQuery(new FuzzyQuery(IndexFields.MINOR_TEXT_SEARCH.term(word)), MINOR_FIELDS_DOWNBOOST));
            builder.add(new BoostQuery(new FuzzyQuery(IndexFields.MINOR_STRINGS_SEARCH.term(word)), MINOR_FIELDS_DOWNBOOST));
        }

        return builder.build()
                .map(query -> new BooleanClause(query, BooleanClause.Occur.SHOULD));
    }

}
