package io.atherios.atherioshr.candidates;

import java.time.LocalDate;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

@Getter
public class JsonCandidate {

    @NotNull
    @NotBlank
    private final String firstName;

    @NotNull
    @NotBlank
    private final String lastName;

    @NotNull
    private final LocalDate dateOfBirth;

    @Email
    @NotNull
    private final String email;

    @NotNull
    @NotBlank
    private final String phone;

    @NotNull
    @NotBlank
    private String address;

    @NotNull
    @NotBlank
    private String evidenceNumber;

    @JsonCreator
    public JsonCandidate(@JsonProperty("first_name") String firstName,
                         @JsonProperty("last_name") String lastName,
                         @JsonProperty("date_of_birth") LocalDate dateOfBirth,
                         @JsonProperty("email") String email,
                         @JsonProperty("phone") String phone,
                         @JsonProperty("address") String address,
                         @JsonProperty("evidence_number") String evidenceNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.email = email;
        this.phone = phone;
        this.address = address;
        this.evidenceNumber = evidenceNumber;
    }

    public JsonCandidate(Candidate candidate) {
        this.firstName = candidate.getFirstName();
        this.lastName = candidate.getLastName();
        this.dateOfBirth = candidate.getDateOfBirth();
        this.email = candidate.getEmail();
        this.phone = candidate.getPhone();
        this.address = candidate.getAddress();
        this.evidenceNumber = candidate.getEvidenceNumber();
    }
}
