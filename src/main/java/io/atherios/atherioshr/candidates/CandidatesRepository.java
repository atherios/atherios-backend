package io.atherios.atherioshr.candidates;

import io.atherios.atherioshr.persistence.AtheriosRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CandidatesRepository extends AtheriosRepository<Candidate, Integer> {

    @Query("select c from Candidate c order by c.lastName, c.firstName")
    Page<Candidate> searchCandidates(Pageable pageable);

    @Query("select c from Candidate c where upper(c.firstName || ' ' || c.lastName) like upper(:phrase)")
    List<Candidate> searchCandidates(@Param("phrase") String phrase);

}
