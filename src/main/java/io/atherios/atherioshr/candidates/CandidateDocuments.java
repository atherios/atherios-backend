package io.atherios.atherioshr.candidates;

import com.google.common.primitives.Ints;
import io.atherios.atherioshr.lucene.AtheriosDocument;
import io.atherios.atherioshr.lucene.AtheriosDocuments;
import io.atherios.atherioshr.lucene.LuceneDocumentBuilder;
import io.atherios.atherioshr.lucene.index.IndexFields;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@Component
@AllArgsConstructor
@Slf4j
public class CandidateDocuments implements AtheriosDocuments {

    private final CandidatesRepository candidatesRepository;

    @Override
    public Stream<AtheriosDocument> allDocuments() {
        List<Candidate> candidates = candidatesRepository.findAll();
        log.info("Found {} candidates to index.", candidates.size());
        return candidates.stream()
                .map(CandidateDocuments::asLuceneDocument);
    }

    private static AtheriosDocument asLuceneDocument(Candidate candidate) {
        return LuceneDocumentBuilder.builder(Candidate.class, candidate.getId())
                .withField(IndexFields.TEXT_SEARCH, candidate.getFirstName())
                .withField(IndexFields.TEXT_SEARCH, candidate.getLastName())
                .withField(IndexFields.TEXT_SEARCH, candidate.getAddress())
                .withEmailField(IndexFields.STRINGS_SEARCH, candidate.getEmail())
                .build();
    }

    @Override
    public Optional<Object> findSearchHit(String id) {
        return candidatesRepository.tryFind(Ints.tryParse(id))
                .map(CandidateListItem::new);
    }

    @Override
    public Optional<AtheriosDocument> findIndexDocument(String entityId) {
        return candidatesRepository.tryFind(Ints.tryParse(entityId))
                .map(CandidateDocuments::asLuceneDocument);
    }

    @Override
    public String documentType() {
        return Candidate.class.getCanonicalName();
    }
}
