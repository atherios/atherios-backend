package io.atherios.atherioshr.candidates;

import io.atherios.atherioshr.contract.domain.model.Contract;
import io.atherios.atherioshr.contract.service.ContractService;
import io.atherios.atherioshr.recruitment.candidates.RecruitedCandidateService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/candidates")
@AllArgsConstructor
public class CandidatesController {

    private final CandidateService candidateService;
    private final RecruitedCandidateService recruitedCandidateService;
    private final ContractService contractService;

    @GetMapping
    public Page<CandidateListItem> listCandidates(@RequestParam("limit") int limit, @RequestParam("page") int page) {
        return candidateService.listAll(new PageRequest(page, limit));
    }

    @GetMapping("/{candidateId}")
    public CandidateListItem findOne(@PathVariable("candidateId") int candidateId) {
        return candidateService.findOne(candidateId);
    }

    @GetMapping("/{candidateId}/recruitment")
    public Object candidateRecruitment(@PathVariable("candidateId") int candidateId) {
        return recruitedCandidateService.findByCandidate(candidateId);
    }

    @GetMapping("/{candidateId}/contracts")
    public List<Contract> getListContractsByCandidate(@PathVariable("candidateId") int candidateId) {
        return contractService.listAllByCandidate(candidateId);
    }

    @GetMapping("/search")
    public List<CandidateSearchHint> searchCandidates(@RequestParam("phrase") String phrase) {
        return candidateService.searchCandidates(phrase);
    }

    @PostMapping
    public ResponseEntity save(@RequestBody JsonCandidate jsonCandidate) {
        candidateService.save(jsonCandidate);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PutMapping("/{candidateId}")
    public ResponseEntity save(@PathVariable("candidateId") int candidateId,
                               @RequestBody JsonCandidate jsonCandidate) {
        candidateService.update(candidateId, jsonCandidate);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{candidateId}")
    public ResponseEntity delete(@PathVariable("candidateId") int candidateId) {
        candidateService.delete(candidateId);
        return ResponseEntity.noContent().build();
    }


}
