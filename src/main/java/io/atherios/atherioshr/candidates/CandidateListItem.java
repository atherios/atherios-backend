package io.atherios.atherioshr.candidates;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import lombok.Getter;

@Getter
public class CandidateListItem {

    private final Integer id;
    @JsonUnwrapped
    private final JsonCandidate candidate;

    public CandidateListItem(Candidate candidate) {
        this.id = candidate.getId();
        this.candidate = new JsonCandidate(candidate);
    }

}
