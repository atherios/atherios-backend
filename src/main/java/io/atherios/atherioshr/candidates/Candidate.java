package io.atherios.atherioshr.candidates;

import java.time.LocalDate;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Email;

@Entity
@Table(name = "candidates")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class Candidate {

    @Id
    @Column(name = "person_id")
    @GeneratedValue(generator = "candidates_gen")
    @SequenceGenerator(name = "candidates_gen", sequenceName = "candidates_seq")
    private Integer id;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(nullable = false)
    private LocalDate dateOfBirth;

    private String address;
    private String evidenceNumber;

    @Email
    @Column(nullable = false)
    private String email;

    @Column(name = "phone")
    private String phone;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Candidate)) return false;
        Candidate candidate = (Candidate) o;
        return Objects.equals(getId(), candidate.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
