package io.atherios.atherioshr.candidates;

import lombok.Getter;

@Getter
public class CandidateSearchHint {

    private final int id;
    private final String fullName;

    public CandidateSearchHint(Candidate candidate) {
        this.id = candidate.getId();
        this.fullName = candidate.getFirstName() + ' ' + candidate.getLastName();
    }
}
