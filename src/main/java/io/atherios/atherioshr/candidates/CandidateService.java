package io.atherios.atherioshr.candidates;

import com.google.common.collect.ImmutableList;
import io.atherios.atherioshr.lucene.DocumentChangedEvent;
import io.atherios.atherioshr.lucene.search.DocumentIdentity;
import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@Validated
@AllArgsConstructor
public class CandidateService {

    private final CandidatesRepository candidatesRepository;
    private final ApplicationEventPublisher applicationEventPublisher;

    public Page<CandidateListItem> listAll(Pageable pageable) {
        return candidatesRepository.searchCandidates(pageable)
                                   .map(CandidateListItem::new);
    }

    public CandidateListItem findOne(int candidateId) {
        Candidate candidate = candidatesRepository.find(candidateId);
        return new CandidateListItem(candidate);
    }

    public void save(@Valid JsonCandidate jsonCandidate) {
        Candidate candidate = Candidate.builder()
                                       .firstName(jsonCandidate.getFirstName())
                                       .lastName(jsonCandidate.getLastName())
                                       .dateOfBirth(jsonCandidate.getDateOfBirth())
                                       .email(jsonCandidate.getEmail())
                                       .phone(jsonCandidate.getPhone())
                                       .address(jsonCandidate.getAddress()).evidenceNumber(jsonCandidate.getAddress())
                                       .build();

        candidatesRepository.save(candidate);
        applicationEventPublisher.publishEvent(new DocumentChangedEvent(new DocumentIdentity(Candidate.class, candidate.getId())));
    }

    public void update(int candidateId, @Valid JsonCandidate jsonCandidate) {
        Candidate candidate = candidatesRepository.find(candidateId);

        candidate.setFirstName(jsonCandidate.getFirstName());
        candidate.setLastName(jsonCandidate.getLastName());
        candidate.setDateOfBirth(jsonCandidate.getDateOfBirth());
        candidate.setEmail(jsonCandidate.getEmail());
        candidate.setPhone(jsonCandidate.getPhone());
        candidate.setAddress(jsonCandidate.getAddress());
        candidate.setEvidenceNumber(jsonCandidate.getEvidenceNumber());
        applicationEventPublisher.publishEvent(new DocumentChangedEvent(new DocumentIdentity(Candidate.class, candidate.getId())));
    }

    public void delete(int candidateId) {
        Candidate candidate = candidatesRepository.find(candidateId);
        candidatesRepository.delete(candidate);
        applicationEventPublisher.publishEvent(new DocumentChangedEvent(new DocumentIdentity(Candidate.class, candidate.getId())));
    }

    public List<CandidateSearchHint> searchCandidates(String phrase) {
        if(phrase.length() < 3) {
            return ImmutableList.of();
        }

        return candidatesRepository.searchCandidates('%' + phrase + '%').stream()
                                   .map(CandidateSearchHint::new)
                                   .collect(Collectors.toList());
    }
}
