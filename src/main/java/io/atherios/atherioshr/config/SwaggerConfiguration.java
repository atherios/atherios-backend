package io.atherios.atherioshr.config;

import static com.google.common.collect.Lists.newArrayList;
import static springfox.documentation.builders.PathSelectors.regex;

import io.atherios.atherioshr.AtheriosHrApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.BasicAuth;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Working swagger links. When application is running:
 * JSON - http://localhost:8080/api/v2/api-docs
 * GUI - http://localhost:8080/api/swagger-ui.html
 */
@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

    @Bean
    public Docket vibanGenerator() {
        return new Docket(DocumentationType.SWAGGER_2)
                .useDefaultResponseMessages(false)
                .select()
                .apis(RequestHandlerSelectors.basePackage(AtheriosHrApplication.class.getPackage().getName()))
                .paths(regex("/.*"))
                .build()
                .apiInfo(apiInfo())
                .securitySchemes(newArrayList(new BasicAuth("xBasic"), new ApiKey("X-Auth-Token", "xAuthToken", "header")))
                .securityContexts(newArrayList(xBasicSecurityContext(), xAuthTokenSecurityContext()));
    }

    private SecurityContext xBasicSecurityContext() {
        return SecurityContext.builder()
                       .securityReferences(newArrayList(new SecurityReference("xBasic", new AuthorizationScope[0])))
                       .build();
    }

    private SecurityContext xAuthTokenSecurityContext() {
        return SecurityContext.builder()
                       .securityReferences(newArrayList(new SecurityReference("xAuthToken", new AuthorizationScope[0])))
                       .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Atherios Hr Appliaction")
                .version("1.0")
                .contact(new Contact("Super dev team", "", ""))
                .build();
    }

}