package io.atherios.atherioshr.recruitment;

import io.atherios.atherioshr.recruitment.states.RecruitmentState;
import lombok.*;

import javax.persistence.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "recruitments")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class Recruitment {

    @Id
    @Column(name = "recrutment_id")
    @GeneratedValue(generator = "recruitments_gen")
    @SequenceGenerator(name = "recruitments_gen", sequenceName = "recruitments_seq")
    private Integer id;

    @Column(name = "summary", nullable = false, length = 250)
    private String summary;

    @Column(name = "start_date", nullable = false)
    private Instant startDate;

    @Column(name = "end_date")
    private Instant endDate;

    @OneToMany(mappedBy = "recruitment", orphanRemoval = true, cascade = CascadeType.ALL)
    @OrderColumn(name = "ordinal", nullable = false)
    private List<RecruitmentState> states;

    public List<RecruitmentState> getStates() {
        if (states == null)
            states = new ArrayList<>();
        return states;
    }

    public void setStates(List<RecruitmentState> states) {
        getStates().clear();
        getStates().addAll(states);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Recruitment)) return false;
        Recruitment that = (Recruitment) o;
        return Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getId());
    }
}
