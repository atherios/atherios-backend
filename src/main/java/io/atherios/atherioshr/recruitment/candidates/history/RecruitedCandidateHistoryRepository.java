package io.atherios.atherioshr.recruitment.candidates.history;

import java.util.List;

import io.atherios.atherioshr.persistence.AtheriosRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface RecruitedCandidateHistoryRepository extends AtheriosRepository<RecruitedCandidateHistory, Integer> {

    @Query("select h from RecruitedCandidateHistory h where h.recruitedCandidate.id = :recruitedCandidate")
    List<RecruitedCandidateHistory> findByRecrutiedCandidate(@Param("recruitedCandidate") int recruitedCandidate);

}
