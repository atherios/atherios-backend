package io.atherios.atherioshr.recruitment.candidates.history;

import io.atherios.atherioshr.auth.user.profile.JsonUserProfile;
import lombok.Getter;

import java.time.Instant;

@Getter
public class RecruitedCandidateHistoryListItem {

    private final int previousStateId;
    private final int nextStateId;
    private final JsonUserProfile user;
    private final Instant actionTime;

    public RecruitedCandidateHistoryListItem(RecruitedCandidateHistory history) {
        this.previousStateId = history.getPreviousState().getId();
        this.nextStateId = history.getNextState().getId();
        this.user = new JsonUserProfile(history.getUser());
        this.actionTime = history.getTime();
    }


}
