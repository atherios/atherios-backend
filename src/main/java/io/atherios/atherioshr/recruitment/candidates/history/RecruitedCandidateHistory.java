package io.atherios.atherioshr.recruitment.candidates.history;

import java.time.Instant;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import io.atherios.atherioshr.auth.user.User;
import io.atherios.atherioshr.recruitment.candidates.RecruitedCandidate;
import io.atherios.atherioshr.recruitment.states.RecruitmentState;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Builder
@Table(name = "recruitment_history")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class RecruitedCandidateHistory {

    @Id
    @Column(name = "recruitment_history_id")
    @GeneratedValue(generator = "recruitment_history_gen")
    @SequenceGenerator(name = "recruitment_history_gen", sequenceName = "recruitment_history_seq")
    private Integer id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "previous_state_id")
    private RecruitmentState previousState;

    @ManyToOne(optional = false)
    @JoinColumn(name = "next_state_id")
    private RecruitmentState nextState;

    @ManyToOne(optional = false)
    @JoinColumn(name = "recruitment_id")
    private RecruitedCandidate recruitedCandidate;

    @Column(name = "action_time", nullable = false)
    private Instant time;

    @JoinColumn(name = "user_id")
    @ManyToOne(optional = false)
    private User user;

}
