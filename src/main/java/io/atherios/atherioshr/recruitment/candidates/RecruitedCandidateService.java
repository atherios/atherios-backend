package io.atherios.atherioshr.recruitment.candidates;

import java.time.Instant;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import javax.validation.Valid;

import io.atherios.atherioshr.auth.AtheriosAuthentication;
import io.atherios.atherioshr.auth.user.User;
import io.atherios.atherioshr.auth.user.UsersRepository;
import io.atherios.atherioshr.candidates.Candidate;
import io.atherios.atherioshr.candidates.CandidatesRepository;
import io.atherios.atherioshr.recruitment.Recruitment;
import io.atherios.atherioshr.recruitment.RecruitmentRepository;
import io.atherios.atherioshr.recruitment.candidates.history.RecruitedCandidateHistory;
import io.atherios.atherioshr.recruitment.candidates.history.RecruitedCandidateHistoryListItem;
import io.atherios.atherioshr.recruitment.candidates.history.RecruitedCandidateHistoryRepository;
import io.atherios.atherioshr.recruitment.states.RecruitmentState;
import io.atherios.atherioshr.recruitment.states.RecruitmentStatesRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

@Service
@Transactional
@Validated
@AllArgsConstructor
public class RecruitedCandidateService {

    private final RecruitmentRepository recruitmentRepository;
    private final RecruitedCandidateRepository recruitedCandidateRepository;
    private final CandidatesRepository candidatesRepository;
    private final RecruitmentStatesRepository recruitmentStatesRepository;
    private final RecruitedCandidateHistoryRepository recruitedCandidateHistoryRepository;
    private final UsersRepository usersRepository;

    public List<RecruitedCandidateListItem> findByCandidate(int candidateId) {
        List<RecruitedCandidate> recruitment = recruitedCandidateRepository.findByCandidate(candidateId);
        return recruitment.stream()
                          .map(RecruitedCandidateListItem::new)
                          .collect(Collectors.toList());
    }

    public void assignCandidate(int recruitmentId, @Valid JsonRecruitedCandidate jsonRecruitedCandidate) {
        Recruitment recruitment = recruitmentRepository.find(recruitmentId);
        RecruitmentState state = recruitmentStatesRepository.find(jsonRecruitedCandidate.getStateId());
        Candidate candidate = candidatesRepository.find(jsonRecruitedCandidate.getCandidateId());

        RecruitedCandidate recruitedCandidate = new RecruitedCandidate(recruitment, candidate, state);
        recruitedCandidateRepository.save(recruitedCandidate);
    }

    public void switchState(int recruitmentId, @Valid JsonRecruitedCandidate jsonRecruitedCandidate) {
        RecruitedCandidate recruitedCandidate =
                recruitedCandidateRepository.findByRecruitmentAndCandidate(recruitmentId, jsonRecruitedCandidate.getCandidateId());
        RecruitmentState state = recruitmentStatesRepository.find(jsonRecruitedCandidate.getStateId());

        RecruitedCandidateHistory historyEntry = RecruitedCandidateHistory.builder()
                                                                          .previousState(recruitedCandidate.getState())
                                                                          .nextState(state)
                                                                          .recruitedCandidate(recruitedCandidate)
                                                                          .time(Instant.now())
                                                                          .user(loggedUser())
                                                                          .build();

        recruitedCandidateHistoryRepository.save(historyEntry);
        recruitedCandidate.setState(state);
    }

    public void dismissCandidate(int recruitmentId, int candidateId) {
        RecruitedCandidate candidate = recruitedCandidateRepository.findByRecruitmentAndCandidate(recruitmentId, candidateId);
        recruitedCandidateRepository.delete(candidate);
    }

    public List<RecruitedCandidateHistoryListItem> findHistory(int recruitedCandidateId) {
        return recruitedCandidateHistoryRepository.findByRecrutiedCandidate(recruitedCandidateId)
                                                  .stream()
                                                  .sorted(Comparator.comparing(RecruitedCandidateHistory::getTime).reversed())
                                                  .map(RecruitedCandidateHistoryListItem::new)
                                                  .collect(Collectors.toList());
    }

    private User loggedUser() {
        AtheriosAuthentication user = (AtheriosAuthentication) SecurityContextHolder.getContext().getAuthentication();
        return usersRepository.find(user.getName());
    }
}
