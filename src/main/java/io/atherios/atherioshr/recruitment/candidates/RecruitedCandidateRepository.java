package io.atherios.atherioshr.recruitment.candidates;

import io.atherios.atherioshr.persistence.AtheriosRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RecruitedCandidateRepository extends AtheriosRepository<RecruitedCandidate, RecruitedCandidate.PK> {

    @Query("select c from RecruitedCandidate c where c.recruitment.id = :recruitmentId and c.state.id = :stateId")
    List<RecruitedCandidate> findByRecruitmentAndState(@Param("recruitmentId") int recruitmentId, @Param("stateId") int stateId);

    @Query("select c from RecruitedCandidate c where c.recruitment.id = :recruitmentId and c.candidate.id = :candidateId")
    RecruitedCandidate findByRecruitmentAndCandidate(@Param("recruitmentId") int recruitmentId, @Param("candidateId") int candidateId);

    @Query("select c from RecruitedCandidate c where c.state.id = :stateId")
    List<RecruitedCandidate> findByState(@Param("stateId") int stateId);

    @Query("select c from RecruitedCandidate c where c.candidate.id = :candidateId")
    List<RecruitedCandidate> findByCandidate(@Param("candidateId") int candidateId);
}
