package io.atherios.atherioshr.recruitment.candidates;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class JsonRecruitedCandidate {

    private final int candidateId;
    private final int stateId;

    @JsonCreator
    public JsonRecruitedCandidate(@JsonProperty("candidate_id") int candidateId,
                                  @JsonProperty("state_id") int stateId) {
        this.candidateId = candidateId;
        this.stateId = stateId;
    }

}
