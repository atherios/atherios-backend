package io.atherios.atherioshr.recruitment.candidates;

import java.util.List;

import io.atherios.atherioshr.recruitment.candidates.history.RecruitedCandidateHistoryListItem;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@RequestMapping("/recruitment")
public class RecruitedCandidateController {

    private final RecruitedCandidateService recruitedCandidateService;

    @GetMapping("/{recruitmentId}/history")
    public List<RecruitedCandidateHistoryListItem> findHistory(@PathVariable("recruitmentId") int recruitmentId) {
        return recruitedCandidateService.findHistory(recruitmentId);
    }

    @PostMapping("/{recruitmentId}/candidates")
    public ResponseEntity<?> assignCandidate(@PathVariable("recruitmentId") int recruitmentId,
                                             @RequestBody JsonRecruitedCandidate jsonRecruitedCandidate) {
        recruitedCandidateService.assignCandidate(recruitmentId, jsonRecruitedCandidate);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/{recruitmentId}/candidates")
    public ResponseEntity<?> switchState(@PathVariable("recruitmentId") int recruitmentId,
                                         @RequestBody JsonRecruitedCandidate jsonRecruitedCandidate) {
        recruitedCandidateService.switchState(recruitmentId, jsonRecruitedCandidate);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{recruitmentId}/candidates/{candidateId}")
    public ResponseEntity<?> dismissCandidate(@PathVariable("recruitmentId") int recruitmentId,
                                              @PathVariable("candidateId") int candidateId) {
        recruitedCandidateService.dismissCandidate(recruitmentId, candidateId);
        return ResponseEntity.noContent().build();
    }
}
