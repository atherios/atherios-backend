package io.atherios.atherioshr.recruitment.candidates;

import io.atherios.atherioshr.candidates.Candidate;
import io.atherios.atherioshr.recruitment.Recruitment;
import io.atherios.atherioshr.recruitment.states.RecruitmentState;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "recruited_candidate")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RecruitedCandidate {

    @Id
    @Column(name = "recruited_candidate_id")
    @GeneratedValue(generator = "recruited_candidates_gen")
    @SequenceGenerator(name = "recruited_candidates_gen", sequenceName = "recruited_candidates_seq")
    private Integer id;

    @JoinColumn(name = "recruitment_id")
    @ManyToOne(optional = false)
    private Recruitment recruitment;

    @JoinColumn(name = "candidate_id")
    @ManyToOne(optional = false)
    private Candidate candidate;

    @JoinColumn(name = "state_id")
    @ManyToOne(optional = false)
    private RecruitmentState state;

    public RecruitedCandidate(Recruitment recruitment, Candidate candidate, RecruitmentState state) {
        this.recruitment = recruitment;
        this.candidate = candidate;
        this.state = state;
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Getter
    @Setter
    static class PK implements Serializable {

        private int candidate;
        private int recruitment;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof PK)) return false;
            PK pk = (PK) o;
            return getCandidate() == pk.getCandidate() &&
                    getRecruitment() == pk.getRecruitment();
        }

        @Override
        public int hashCode() {
            return Objects.hash(getCandidate(), getRecruitment());
        }
    }
}
