package io.atherios.atherioshr.recruitment.candidates;

import io.atherios.atherioshr.recruitment.RecruitmentListItem;
import io.atherios.atherioshr.recruitment.states.RecruitmentStateListItem;
import lombok.Getter;

@Getter
public class RecruitedCandidateListItem {

    private final int id;
    private final RecruitmentListItem recruitment;
    private final RecruitmentStateListItem state;

    public RecruitedCandidateListItem(RecruitedCandidate recruitedCandidate) {
        this.id = recruitedCandidate.getId();
        this.recruitment = new RecruitmentListItem(recruitedCandidate.getRecruitment());
        this.state = new RecruitmentStateListItem(recruitedCandidate.getState());
    }
}
