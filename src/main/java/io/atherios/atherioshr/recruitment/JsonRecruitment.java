package io.atherios.atherioshr.recruitment;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.time.Instant;

@Getter
public class JsonRecruitment {

    @Length(max = 250)
    @NotEmpty
    @NotNull
    private String summary;
    private Instant startDate;
    private Instant endDate;

    public JsonRecruitment(Recruitment recruitment) {
        this.summary = recruitment.getSummary();
        this.startDate = recruitment.getStartDate();
        this.endDate = recruitment.getEndDate();
    }

    @JsonCreator
    public JsonRecruitment(@JsonProperty("summary") String summary,
                           @JsonProperty("start_date") Instant startDate,
                           @JsonProperty("end_date") Instant endDate) {
        this.summary = summary;
        this.startDate = startDate;
        this.endDate = endDate;
    }
}
