package io.atherios.atherioshr.recruitment;


import io.atherios.atherioshr.persistence.AtheriosRepository;

public interface RecruitmentRepository extends AtheriosRepository<Recruitment, Integer> {
}
