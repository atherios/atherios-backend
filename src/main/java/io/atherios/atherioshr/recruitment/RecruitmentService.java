package io.atherios.atherioshr.recruitment;

import java.util.List;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import javax.validation.Valid;

import io.atherios.atherioshr.lucene.DocumentChangedEvent;
import io.atherios.atherioshr.lucene.search.DocumentIdentity;
import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

@Service
@Transactional
@Validated
@AllArgsConstructor
public class RecruitmentService {

    private final RecruitmentRepository recruitmentRepository;
    private final ApplicationEventPublisher applicationEventPublisher;

    public List<RecruitmentListItem> listAll() {
        return recruitmentRepository.findAll().stream()
                .map(RecruitmentListItem::new)
                .collect(Collectors.toList());
    }

    public RecruitmentListItem findOne(int recruitmentId) {
        Recruitment recruitment = recruitmentRepository.findOne(recruitmentId);
        return new RecruitmentListItem(recruitment);
    }

    public void create(@Valid JsonRecruitment jsonRecruitment) {
        Recruitment recruitment = Recruitment.builder()
                .summary(jsonRecruitment.getSummary())
                .endDate(jsonRecruitment.getEndDate())
                .startDate(jsonRecruitment.getStartDate())
                .build();

        recruitmentRepository.save(recruitment);
        applicationEventPublisher.publishEvent(new DocumentChangedEvent(new DocumentIdentity(Recruitment.class, recruitment.getId())));
    }

    public void update(int id, @Valid JsonRecruitment jsonRecruitment) {
        Recruitment recruitment = recruitmentRepository.find(id);

        recruitment.setEndDate(jsonRecruitment.getEndDate());
        recruitment.setStartDate(jsonRecruitment.getStartDate());
        recruitment.setSummary(jsonRecruitment.getSummary());
        applicationEventPublisher.publishEvent(new DocumentChangedEvent(new DocumentIdentity(Recruitment.class, recruitment.getId())));
    }

    public void delete(int id) {
        Recruitment recruitment = recruitmentRepository.find(id);
        recruitmentRepository.delete(recruitment);
        applicationEventPublisher.publishEvent(new DocumentChangedEvent(new DocumentIdentity(Recruitment.class, recruitment.getId())));
    }

}
