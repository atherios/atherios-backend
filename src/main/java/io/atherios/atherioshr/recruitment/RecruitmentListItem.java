package io.atherios.atherioshr.recruitment;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import lombok.Getter;

@Getter
public class RecruitmentListItem {

    private final int id;
    @JsonUnwrapped
    private final JsonRecruitment recruitment;

    public RecruitmentListItem(Recruitment recruitment) {
        this.id = recruitment.getId();
        this.recruitment = new JsonRecruitment(recruitment);
    }
}
