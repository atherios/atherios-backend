package io.atherios.atherioshr.recruitment;

import com.google.common.primitives.Ints;
import io.atherios.atherioshr.lucene.AtheriosDocument;
import io.atherios.atherioshr.lucene.AtheriosDocuments;
import io.atherios.atherioshr.lucene.LuceneDocumentBuilder;
import io.atherios.atherioshr.lucene.index.IndexFields;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.stream.Stream;

@Component
@AllArgsConstructor
public class RecruitmentDocuments implements AtheriosDocuments {

    private final RecruitmentRepository recruitmentRepository;

    @Override
    public Stream<AtheriosDocument> allDocuments() {
        return recruitmentRepository.findAll().stream()
                .map(this::asDocument);
    }

    @Override
    public Optional<Object> findSearchHit(String id) {
        return recruitmentRepository.tryFind(Ints.tryParse(id))
                .map(RecruitmentListItem::new);
    }

    @Override
    public String documentType() {
        return Recruitment.class.getCanonicalName();
    }

    @Override
    public Optional<AtheriosDocument> findIndexDocument(String entityId) {
        return recruitmentRepository.tryFind(Integer.parseInt(entityId)).map(this::asDocument);
    }

    private AtheriosDocument asDocument(Recruitment recruitment) {
        return LuceneDocumentBuilder.builder(Recruitment.class, recruitment.getId())
                .withField(IndexFields.STRINGS_SEARCH, '#' + recruitment.getId().toString())
                .withField(IndexFields.TEXT_SEARCH, recruitment.getSummary())
                .build();
    }
}
