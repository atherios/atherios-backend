package io.atherios.atherioshr.recruitment.board;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class RecruitmentBoardController {

    private final RecruitmentBoardService recruitmentBoardService;

    @GetMapping("/recruitment/{recruitmentId}/board")
    public JsonRecruitmentBoard showBoard(@PathVariable("recruitmentId") int recruitmentId) {
        return recruitmentBoardService.showBoard(recruitmentId);
    }

}
