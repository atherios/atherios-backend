package io.atherios.atherioshr.recruitment.board;

import com.google.common.collect.ForwardingMap;
import io.atherios.atherioshr.candidates.CandidateListItem;

import java.util.List;
import java.util.Map;

public class JsonRecruitmentBoard extends ForwardingMap<Integer, List<CandidateListItem>> {

    private final Map<Integer, List<CandidateListItem>> delegate;

    public JsonRecruitmentBoard(Map<Integer, List<CandidateListItem>> delegate) {
        this.delegate = delegate;
    }

    @Override
    protected Map<Integer, List<CandidateListItem>> delegate() {
        return delegate;
    }
}
