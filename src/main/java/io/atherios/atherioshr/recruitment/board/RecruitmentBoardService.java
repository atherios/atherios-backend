package io.atherios.atherioshr.recruitment.board;

import com.google.common.collect.ImmutableMap;
import io.atherios.atherioshr.candidates.Candidate;
import io.atherios.atherioshr.candidates.CandidateListItem;
import io.atherios.atherioshr.recruitment.Recruitment;
import io.atherios.atherioshr.recruitment.RecruitmentRepository;
import io.atherios.atherioshr.recruitment.candidates.RecruitedCandidate;
import io.atherios.atherioshr.recruitment.candidates.RecruitedCandidateRepository;
import io.atherios.atherioshr.recruitment.states.RecruitmentState;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class RecruitmentBoardService {

    private static final Comparator<Candidate> NATURAL_CANDIDATES_SORTING = Comparator.comparing(Candidate::getLastName).thenComparing(Candidate::getFirstName);

    private final RecruitedCandidateRepository recruitedCandidateRepository;
    private final RecruitmentRepository recruitmentRepository;

    public JsonRecruitmentBoard showBoard(int recruitmentId) {
        Recruitment recruitment = recruitmentRepository.find(recruitmentId);

        ImmutableMap.Builder<Integer, List<CandidateListItem>> board = ImmutableMap.builder();
        for (RecruitmentState state : recruitment.getStates()) {
            if (state == null)
                continue;
            List<CandidateListItem> candidates = recruitedCandidateRepository.findByRecruitmentAndState(recruitmentId, state.getId()).stream()
                    .map(RecruitedCandidate::getCandidate)
                    .sorted(NATURAL_CANDIDATES_SORTING)
                    .map(CandidateListItem::new)
                    .collect(Collectors.toList());
            board.put(state.getId(), candidates);
        }

        return new JsonRecruitmentBoard(board.build());
    }

}
