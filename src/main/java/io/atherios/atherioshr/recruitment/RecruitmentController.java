package io.atherios.atherioshr.recruitment;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/recruitment")
public class RecruitmentController {

    private final RecruitmentService recruitmentService;

    @GetMapping
    public List<RecruitmentListItem> listAll() {
        return recruitmentService.listAll();
    }

    @GetMapping("/{recruitmentId}")
    public RecruitmentListItem findOne(@PathVariable("recruitmentId") int recruitmentId) {
        return recruitmentService.findOne(recruitmentId);
    }

    @PostMapping
    public void save(@RequestBody JsonRecruitment jsonRecruitment) {
        recruitmentService.create(jsonRecruitment);
    }

    @PutMapping("/{id}")
    public void update(@PathVariable("id") int id,
                       @RequestBody JsonRecruitment jsonRecruitment) {
        recruitmentService.update(id, jsonRecruitment);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") int id) {
        recruitmentService.delete(id);
    }
}
