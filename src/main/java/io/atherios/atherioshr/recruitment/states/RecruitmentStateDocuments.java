package io.atherios.atherioshr.recruitment.states;

import com.google.common.primitives.Ints;
import io.atherios.atherioshr.lucene.AtheriosDocument;
import io.atherios.atherioshr.lucene.AtheriosDocuments;
import io.atherios.atherioshr.lucene.LuceneDocumentBuilder;
import io.atherios.atherioshr.lucene.index.IndexFields;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.stream.Stream;

@Component
@AllArgsConstructor
public class RecruitmentStateDocuments implements AtheriosDocuments {

    private final RecruitmentStatesRepository recruitmentStatesRepository;

    @Override
    public Stream<AtheriosDocument> allDocuments() {
        return recruitmentStatesRepository.findAll().stream()
                .map(this::asDocument);
    }

    @Override
    public Optional<Object> findSearchHit(String id) {
        return recruitmentStatesRepository.tryFind(Ints.tryParse(id))
                .map(RecruitmentStateListItem::new);
    }

    @Override
    public String documentType() {
        return RecruitmentState.class.getCanonicalName();
    }

    @Override
    public Optional<AtheriosDocument> findIndexDocument(String entityId) {
        return recruitmentStatesRepository.tryFind(Ints.tryParse(entityId)).map(this::asDocument);
    }

    private AtheriosDocument asDocument(RecruitmentState recruitmentState) {
        return LuceneDocumentBuilder.builder(RecruitmentState.class, recruitmentState.getId())
                .withField(IndexFields.STRINGS_SEARCH, '#' + recruitmentState.getId().toString())
                .withField(IndexFields.TEXT_SEARCH, recruitmentState.getRecruitment().getSummary())
                .build();
    }
}
