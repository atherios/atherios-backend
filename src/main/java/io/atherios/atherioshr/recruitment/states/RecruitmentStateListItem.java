package io.atherios.atherioshr.recruitment.states;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import lombok.Getter;

@Getter
public class RecruitmentStateListItem {

    private final int id;
    @JsonUnwrapped
    private final JsonRecruitmentState state;
    private final int ordinal;
    private final int recruitmentId;

    public RecruitmentStateListItem(RecruitmentState state) {
        this.id = state.getId();
        this.ordinal = state.getOrdinal();
        this.state = new JsonRecruitmentState(state);
        this.recruitmentId = state.getRecruitment().getId();
    }

}
