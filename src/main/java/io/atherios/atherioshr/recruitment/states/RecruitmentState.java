package io.atherios.atherioshr.recruitment.states;

import io.atherios.atherioshr.recruitment.Recruitment;
import lombok.*;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "recruitment_states")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class RecruitmentState {

    @Id
    @Column(name = "recruitment_state_id")
    @GeneratedValue(generator = "recruitment_states_gen", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "recruitment_states_gen", sequenceName = "recruitment_states_seq")
    private Integer id;

    @Column(name = "state_label", nullable = false)
    private String label;

    @Column(name = "closed", nullable = false)
    private boolean closed;

    @Column(name = "ordinal", nullable = false)
    private int ordinal;

    @ManyToOne(optional = false)
    @JoinColumn(name = "recruitment_id")
    private Recruitment recruitment;

    @Column(name = "options")
    @Convert(converter = RecruitmentStateOptionsConverter.class)
    private RecruitmentStateOptions options = RecruitmentStateOptions.EMPTY;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RecruitmentState)) return false;
        RecruitmentState that = (RecruitmentState) o;
        return Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getId());
    }
}
