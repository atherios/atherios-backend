package io.atherios.atherioshr.recruitment.states;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import javax.validation.Valid;

import io.atherios.atherioshr.lucene.DocumentChangedEvent;
import io.atherios.atherioshr.lucene.search.DocumentIdentity;
import io.atherios.atherioshr.recruitment.Recruitment;
import io.atherios.atherioshr.recruitment.RecruitmentRepository;
import io.atherios.atherioshr.recruitment.candidates.RecruitedCandidate;
import io.atherios.atherioshr.recruitment.candidates.RecruitedCandidateRepository;
import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

@Service
@Validated
@Transactional
@AllArgsConstructor
public class RecruitmentStateService {

    private final RecruitmentRepository recruitmentRepository;
    private final RecruitmentStatesRepository recruitmentStatesRepository;
    private final RecruitedCandidateRepository recruitedCandidateRepository;
    private final ApplicationEventPublisher applicationEventPublisher;

    public List<RecruitmentStateListItem> listStates(int recruitmentId) {
        Recruitment recruitment = recruitmentRepository.find(recruitmentId);

        return recruitment.getStates().stream()
                .filter(Objects::nonNull) // uroki Hibernate'owego @OrderBy
                .sorted(Comparator.comparing(RecruitmentState::getOrdinal))
                .map(RecruitmentStateListItem::new)
                .collect(Collectors.toList());
    }

    public void save(int recruitmentId, @Valid JsonRecruitmentState jsonState) {
        Recruitment recruitment = recruitmentRepository.find(recruitmentId);

        RecruitmentState state = RecruitmentState.builder()
                .recruitment(recruitment)
                .options(jsonState.getOptions())
                .label(jsonState.getLabel())
                .closed(jsonState.isClosed())
                .build();
        recruitment.getStates().add(state);
        recruitmentStatesRepository.flush();
        applicationEventPublisher.publishEvent(new DocumentChangedEvent(new DocumentIdentity(RecruitmentState.class, state.getId())));
    }

    public void update(int stateId, @Valid JsonRecruitmentState jsonState) {
        RecruitmentState state = recruitmentStatesRepository.find(stateId);

        state.setClosed(jsonState.isClosed());
        state.setLabel(jsonState.getLabel());
        state.setOptions(jsonState.getOptions());
        applicationEventPublisher.publishEvent(new DocumentChangedEvent(new DocumentIdentity(RecruitmentState.class, state.getId())));
    }

    public void saveStatesOrder(int recruitmentId, List<Integer> stateIds) {
        Recruitment recruitment = recruitmentRepository.find(recruitmentId);

        List<RecruitmentState> states = stateIds.stream().map(recruitmentStatesRepository::find)
                .collect(Collectors.toList());
        recruitment.setStates(states);
    }

    public void delete(int stateId) {
        List<RecruitedCandidate> candidates = recruitedCandidateRepository.findByState(stateId);
        recruitedCandidateRepository.delete(candidates);
        recruitmentStatesRepository.delete(stateId);
        applicationEventPublisher.publishEvent(new DocumentChangedEvent(new DocumentIdentity(RecruitmentState.class, stateId)));
    }
}
