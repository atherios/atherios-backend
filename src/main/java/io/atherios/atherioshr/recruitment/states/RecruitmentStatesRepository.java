package io.atherios.atherioshr.recruitment.states;

import io.atherios.atherioshr.persistence.AtheriosRepository;

public interface RecruitmentStatesRepository extends AtheriosRepository<RecruitmentState, Integer> {
}
