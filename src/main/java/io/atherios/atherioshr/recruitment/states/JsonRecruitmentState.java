package io.atherios.atherioshr.recruitment.states;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

@Getter
public class JsonRecruitmentState {

    @NotNull
    @NotBlank
    private final String label;
    private final boolean closed;
    private final RecruitmentStateOptions options;

    public JsonRecruitmentState(RecruitmentState state) {
        this.label = state.getLabel();
        this.closed = state.isClosed();
        this.options = state.getOptions();
    }

    @JsonCreator
    public JsonRecruitmentState(@JsonProperty("label") String label,
                                @JsonProperty("closed") boolean closed,
                                @JsonProperty("options") RecruitmentStateOptions options) {
        this.label = label;
        this.closed = closed;
        this.options = options;
    }
}
