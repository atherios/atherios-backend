package io.atherios.atherioshr.recruitment.states;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.google.common.collect.ForwardingMap;
import com.google.common.collect.ImmutableMap;

import java.util.Map;

public class RecruitmentStateOptions extends ForwardingMap<RecruitmentStateOptionType, RecruitmentStateOption> {

    public static final RecruitmentStateOptions EMPTY = new RecruitmentStateOptions(ImmutableMap.of());

    private final Map<RecruitmentStateOptionType, RecruitmentStateOption> delegate;

    @JsonCreator
    public RecruitmentStateOptions(Map<RecruitmentStateOptionType, RecruitmentStateOption> delegate) {
        this.delegate = delegate;
    }

    @Override
    protected Map<RecruitmentStateOptionType, RecruitmentStateOption> delegate() {
        return delegate;
    }
}
