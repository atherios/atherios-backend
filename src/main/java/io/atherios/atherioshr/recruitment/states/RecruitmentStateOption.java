package io.atherios.atherioshr.recruitment.states;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import io.atherios.atherioshr.contract.RequireContractGenerationOption;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.EXISTING_PROPERTY,
        property = "type")
@JsonSubTypes(
        @JsonSubTypes.Type(value = RequireContractGenerationOption.class, name = "REQUIRE_CONTRACT_GENERATION"))
public interface RecruitmentStateOption {

    RecruitmentStateOptionType getType();

    boolean uses(Class<?> resource, int id);
}
