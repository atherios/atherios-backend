package io.atherios.atherioshr.recruitment.states;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class RecruitmentStateOptionsConverter implements AttributeConverter<RecruitmentStateOptions, String> {
    @Override
    public String convertToDatabaseColumn(RecruitmentStateOptions recruitmentStateOptions) {
        return Json.asString(recruitmentStateOptions);
    }

    @Override
    public RecruitmentStateOptions convertToEntityAttribute(String json) {
        return Json.unmarshal(json, RecruitmentStateOptions.class);
    }
}
