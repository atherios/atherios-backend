package io.atherios.atherioshr.recruitment.states;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
public class RecruitmentStateController {

    private final RecruitmentStateService recruitmentStateService;

    @GetMapping("/recruitment/{recruitmentId}/states")
    public List<RecruitmentStateListItem> listStates(@PathVariable("recruitmentId") int recruitmentId) {
        return recruitmentStateService.listStates(recruitmentId);
    }

    @PutMapping("/recruitment/{recruitmentId}/states")
    public ResponseEntity<?> saveStates(@PathVariable("recruitmentId") int recruitmentId,
                                        @RequestBody List<Integer> stateIds) {
        recruitmentStateService.saveStatesOrder(recruitmentId, stateIds);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/recruitment/{recruitmentId}/states")
    public ResponseEntity<?> saveState(@PathVariable("recruitmentId") int recruitmentId,
                                       @RequestBody JsonRecruitmentState state) {
        recruitmentStateService.save(recruitmentId, state);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/recruitmentState/{stateId}")
    public ResponseEntity<?> updateState(@PathVariable("stateId") int stateId,
                                         @RequestBody JsonRecruitmentState state) {
        recruitmentStateService.update(stateId, state);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/recruitmentState/{stateId}")
    public ResponseEntity<?> delete(@PathVariable("stateId") int stateId) {
        recruitmentStateService.delete(stateId);
        return ResponseEntity.ok().build();
    }

}
