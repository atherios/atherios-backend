package io.atherios.atherioshr.files;

import io.atherios.atherioshr.auth.AtheriosAuthentication;
import lombok.AllArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.util.UUID;

import static com.google.common.io.Files.getFileExtension;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

@Service
@AllArgsConstructor
public class FileService {

    private final FileDetailsRepository fileRepository;
    private final BareFilesDirectory bareFilesDirectory;


    @Transactional
    public Integer uploadFile(MultipartFile multipartFile) {
        AtheriosAuthentication user = (AtheriosAuthentication) SecurityContextHolder.getContext().getAuthentication();
        String userName = user.getName();
        Path path = createFile(multipartFile);

        FileDetails fileDetails = FileDetails.builder()
                .mime(multipartFile.getContentType())
                .fileName(multipartFile.getOriginalFilename())
                .physicalFileName(path.getFileName().toString())
                .author(userName)
                .created(Instant.now())
                .size(multipartFile.getSize())
                .build();

        return fileRepository.save(fileDetails).getId();
    }

    public FileDetails moveFile(int fileId, AtheriosDirectory targetPath) {
        FileDetails fileDetails = fileRepository.find(fileId);

        Path current = bareFilesDirectory.resolveFile(fileDetails);
        if (!Files.exists(current)) {
            throw new RuntimeException("Error. The file has already been moved.");
        }
        Path target = targetPath.resolveFile(fileDetails);

        try {
            Files.move(current, target);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }

        return fileDetails;
    }

    private Path createFile(MultipartFile file) {
        try {
            Path path = bareFilesDirectory.path().resolve(UUID.randomUUID().toString() + '.' + getFileExtension(file.getOriginalFilename()));
            Files.copy(file.getInputStream(), path, REPLACE_EXISTING);
            return path;
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }

    }
}
