package io.atherios.atherioshr.files;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;

public interface AtheriosDirectory {

    static Path createDirectory(Path directory) {
        try {
            Files.createDirectories(directory);
            return directory;
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    default Path resolveFile(FileDetails fileDetails) {
        return path().resolve(fileDetails.getPhysicalFileName());
    }

    default Optional<Path> findFile(FileDetails fileDetails) {
        Path file = path().resolve(fileDetails.getPhysicalFileName());
        if (!Files.exists(file))
            return Optional.empty();
        return Optional.of(file);
    }

    default void deleteFile(FileDetails fileDetails) {
        try {
            Files.deleteIfExists(resolveFile(fileDetails));
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    Path path();
}
