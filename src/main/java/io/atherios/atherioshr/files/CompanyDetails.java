package io.atherios.atherioshr.files;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Getter
public class CompanyDetails {

    private final String companyName;
    private final String companyAddress;

    public CompanyDetails(@Value("${contract.company.name}") String companyName,
                          @Value("${contract.company.address}") String companyAddress) {
        this.companyName = companyName;
        this.companyAddress = companyAddress;
    }
}
