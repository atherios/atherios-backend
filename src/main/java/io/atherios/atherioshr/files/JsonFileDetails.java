package io.atherios.atherioshr.files;

import lombok.Getter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.time.Instant;

@Getter
public class JsonFileDetails {

    @NotBlank
    private String fileName;

    @NotBlank
    private String author;

    @NotNull
    private Instant created;

    private long size;

    public JsonFileDetails(FileDetails fileDetails) {
        this.fileName = fileDetails.getFileName();
        this.author = fileDetails.getAuthor();
        this.created = fileDetails.getCreated();
        this.size = fileDetails.getSize();
    }
}
