package io.atherios.atherioshr.files;

import io.atherios.atherioshr.persistence.AtheriosRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Set;

public interface FileDetailsRepository extends AtheriosRepository<FileDetails, Integer> {

    @Query("select f from FileDetails f where f.id in :ids")
    Set<FileDetails> getAllbyIds(@Param("ids") Set<Integer> ids);
}
