package io.atherios.atherioshr.files;

import java.time.Instant;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "file_details")
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
@Getter
@Setter
public class FileDetails {

    @Id
    @Column(name = "file_id")
    @GeneratedValue(generator = "file_gen")
    @SequenceGenerator(name = "file_gen", sequenceName = "file_seq")
    private Integer id;

    @Column(name = "mime", nullable = false)
    private String mime;

    @Column(name = "file_name", nullable = false)
    private String fileName;

    @Column(nullable = false)
    private String author;

    @Column(nullable = false)
    private Instant created;

    private long size;

    @Column(nullable = false)
    private String physicalFileName;

    public FileDetails(String fileName, String author, Instant created, Long size, String physicalFileName) {
        this.fileName = fileName;
        this.author = author;
        this.created = created;
        this.size = size;
        this.physicalFileName = physicalFileName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FileDetails)) return false;
        FileDetails file = (FileDetails) o;
        return Objects.equals(getId(), file.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
