package io.atherios.atherioshr.files;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.UUID;

@Component
public class PdfCreator {

    public File createPdf(Path folderPath, Object objectToProcess, String templateFilePath) {
        try {
            InputStream template = new FileInputStream(templateFilePath);
            JasperReport report = JasperCompileManager.compileReport(template);
            JRDataSource beanColDataSource = new JRBeanCollectionDataSource(Collections.singletonList(objectToProcess));

            File pdfOutputFile = createFile(folderPath);
            JasperPrint print = JasperFillManager.fillReport(report, null, beanColDataSource);
            JasperExportManager.exportReportToPdfStream(print, new FileOutputStream(pdfOutputFile));
            return pdfOutputFile;
        } catch (JRException | FileNotFoundException e) {
            throw new RuntimeException("Error while generating contract pdf", e);
        }

    }

    private File createFile(Path folderPath) {
        String fileName = UUID.randomUUID().toString().replaceAll("-", "");
        try {
            Files.createDirectories(folderPath);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
        return new File(folderPath + "/" + fileName + ".pdf");
    }
}
