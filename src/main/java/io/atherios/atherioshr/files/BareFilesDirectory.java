package io.atherios.atherioshr.files;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.nio.file.Path;

@Component
public class BareFilesDirectory implements AtheriosDirectory {

    private final Path directoryPath;

    public BareFilesDirectory(@Value("${files.folder.path}") Path directoryPath) {
        this.directoryPath = AtheriosDirectory.createDirectory(directoryPath);
    }

    @Override
    public Path path() {
        return directoryPath;
    }
}
