package io.atherios.atherioshr.files;

import com.google.common.collect.ImmutableMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/files")
public class FileController {

    private final FileService fileService;

    public FileController(FileService fileService) {
        this.fileService = fileService;
    }

    @PostMapping
    public Object uploadContract(@RequestParam("file") MultipartFile file) {

        Integer fileId = fileService.uploadFile(file);

        return ImmutableMap.of("file_id", fileId);
    }
}
