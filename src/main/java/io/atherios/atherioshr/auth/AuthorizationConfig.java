package io.atherios.atherioshr.auth;

import org.springframework.boot.autoconfigure.security.Http401AuthenticationEntryPoint;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.WebSecurityConfigurer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class AuthorizationConfig {

    @Bean
    public WebSecurityConfigurer<?> configurer(AtheriosAuthenticationProvider atheriosAuthenticationProvider) {
        return new WebSecurityConfigurerAdapter() {

            @Override
            protected void configure(AuthenticationManagerBuilder auth) {
                auth.authenticationProvider(atheriosAuthenticationProvider);
            }

            @Override
            public void configure(WebSecurity web) throws Exception {
                web.ignoring().antMatchers("/v2/api-docs",
                        "/configuration/ui",
                        "/swagger-resources/**",
                        "/configuration/**",
                        "/swagger-ui.html",
                        "/webjars/**");
            }

            @Override
            protected void configure(HttpSecurity http) throws Exception {
                http.headers()
                        .cacheControl()
                        .disable()
                        .and()
                        .exceptionHandling()
                        .authenticationEntryPoint(new Http401AuthenticationEntryPoint(""))
                        .and()
                        .formLogin().loginPage("/login")
                        .failureHandler((request, response, exception) -> {
                            if (!response.isCommitted())
                                response.setStatus(401);
                        })
                        .successHandler((request, response, authentication) -> {
                            if (!response.isCommitted())
                                response.setStatus(200);
                        }).and()
                        .logout().logoutUrl("/logout")
                        .logoutSuccessHandler((request, response, authentication) -> {
                            if (!response.isCommitted()) {
                                response.setStatus(200);
                            }
                        })
                        .and()
                        .csrf().disable()
                        .authorizeRequests()
                        .antMatchers("/login").permitAll()
                        .antMatchers("/**").fullyAuthenticated();
            }
        };
    }

    @Profile("cors")
    public static class CorsConfiguration {
        
        @Bean
        public WebSecurityConfigurer<?> corsConfigurer() {
            return new WebSecurityConfigurerAdapter() {

                @Override
                protected void configure(HttpSecurity http) throws Exception {
                    http.cors().configurationSource(corsConfiguration());
                }
            };
        }

        private static UrlBasedCorsConfigurationSource corsConfiguration() {
            org.springframework.web.cors.CorsConfiguration config = new org.springframework.web.cors.CorsConfiguration();
            config.setAllowCredentials(true);
            config.addAllowedOrigin("*");
            config.addAllowedHeader("*");
            config.addAllowedMethod("*");

            UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
            source.registerCorsConfiguration("/**", config);
            return source;
        }
    }



}
