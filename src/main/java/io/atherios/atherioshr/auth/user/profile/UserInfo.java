package io.atherios.atherioshr.auth.user.profile;

import com.google.common.hash.Hashing;
import io.atherios.atherioshr.auth.user.User;
import io.atherios.atherioshr.auth.user.UserRole;
import lombok.Getter;

import java.nio.charset.StandardCharsets;
import java.util.Set;

@Getter
public class UserInfo {

    private final String username;
    private final String avatar;
    private final Set<UserRole> roles;

    @SuppressWarnings("deprecation")
    public UserInfo(User authentication) {
        this.username = authentication.getUsername();
        this.avatar = "https://s.gravatar.com/avatar/" + Hashing.md5().hashString(authentication.getUsername() + "@leers.pl", StandardCharsets.UTF_8);
        this.roles = authentication.getRoles();
    }

}
