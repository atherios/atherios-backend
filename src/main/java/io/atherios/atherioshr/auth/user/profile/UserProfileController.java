package io.atherios.atherioshr.auth.user.profile;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class UserProfileController {

    private final UserProfileService userProfileService;

    @GetMapping("/users/current")
    public UserInfo userProfile() {
        return userProfileService.currentUserProfile();
    }

}
