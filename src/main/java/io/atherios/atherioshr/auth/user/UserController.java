package io.atherios.atherioshr.auth.user;

import io.atherios.atherioshr.auth.AtheriosAuthentication;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    @GetMapping
    @PreAuthorize("hasAnyRole('ROLE_HR_WORKER', 'ROLE_ADMIN')")
    public List<UserListItem> listAll() {
        return userService.listAll();
    }

    @GetMapping("/{userId}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public UserListItem findOne(@PathVariable("userId") String userId) {
        return userService.findOne(userId);
    }

    @PostMapping
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void registerUser(@RequestBody UserRegistration userRegistration) {
        userService.registerUser(userRegistration);
    }

    @PutMapping("/{userId}/password")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void changeUserPassword(@PathVariable("userId") String userId, @RequestParam("password") String password) {
        userService.changeUserPassword(userId, password);
    }

    @PutMapping("/current/password")
    public void changeUserPassword(@RequestParam("password") String password) {
        AtheriosAuthentication user = (AtheriosAuthentication) SecurityContextHolder.getContext().getAuthentication();
        userService.changeUserPassword(user.getName(), password);
    }

    @PutMapping("/{userId}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void updateUser(@PathVariable("userId") String userId,
                           @RequestBody JsonUser jsonUser) {
        userService.updateUser(userId, jsonUser);
    }

}
