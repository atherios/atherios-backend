package io.atherios.atherioshr.auth.user;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import org.hibernate.validator.constraints.NotBlank;

import java.util.Set;

@Getter
public class JsonUser {

    @NotNull
    @NotBlank
    private final String username;

    @NotNull
    @NotBlank
    private final String firstName;

    @NotNull
    @NotBlank
    private final String lastName;

    @NotNull
    @NotBlank
    private final String evidenceNumber;

    @NotNull
    private final boolean blocked;

    @NotNull
    private Set<UserRole> roles;

    @JsonCreator
    public JsonUser(@JsonProperty("username") String username,
                    @JsonProperty("first_name") String firstName,
                    @JsonProperty("last_name") String lastName,
                    @JsonProperty("evidence_number") String evidenceNumber,
                    @JsonProperty("blocked") boolean blocked,
                    @JsonProperty("roles") Set<UserRole> roles) {
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.evidenceNumber = evidenceNumber;
        this.blocked = blocked;
        this.roles = roles;
    }

    public JsonUser(User user) {
        this.username = user.getUsername();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.evidenceNumber = user.getEvidenceNumber();
        this.blocked = user.isBlocked();
        this.roles = user.getRoles();
    }

}
