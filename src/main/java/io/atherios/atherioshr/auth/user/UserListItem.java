package io.atherios.atherioshr.auth.user;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import lombok.Getter;

@Getter
public class UserListItem {

    @JsonUnwrapped
    private final JsonUser user;

    public UserListItem(User user) {
        this.user = new JsonUser(user);
    }

}
