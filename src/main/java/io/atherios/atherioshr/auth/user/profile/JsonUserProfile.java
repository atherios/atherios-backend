package io.atherios.atherioshr.auth.user.profile;

import io.atherios.atherioshr.auth.user.User;
import lombok.Getter;

@Getter
public class JsonUserProfile {

    private final String username;
    private final String firstName;
    private final String lastName;

    public JsonUserProfile(User authentication) {
        this.username = authentication.getUsername();
        this.firstName = authentication.getFirstName();
        this.lastName = authentication.getLastName();
    }

}
