package io.atherios.atherioshr.auth.user;

import org.springframework.security.core.GrantedAuthority;

public enum UserRole implements GrantedAuthority {

    ROLE_USER,
    ROLE_HR_WORKER,
    ROLE_ADMIN;

    @Override
    public String getAuthority() {
        return name();
    }

}
