package io.atherios.atherioshr.auth.user;

import io.atherios.atherioshr.AtheriosException;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

import static io.atherios.atherioshr.auth.user.UserRole.ROLE_ADMIN;

@Service
@AllArgsConstructor
@Validated
@Transactional
public class UserService {

    private final UsersRepository usersRepository;
    private final PasswordEncoder passwordEncoder;

    public List<UserListItem> listAll() {
        return usersRepository.findAll().stream()
                .map(UserListItem::new)
                .collect(Collectors.toList());
    }

    public UserListItem findOne(String userId) {
        User user = usersRepository.findOne(userId);
        return new UserListItem(user);
    }

    public void registerUser(@Valid UserRegistration userRegistration) {
        JsonUser jsonUser = userRegistration.getUser();
        User user = User.builder()
                .evidenceNumber(jsonUser.getEvidenceNumber())
                .firstName(jsonUser.getFirstName())
                .lastName(jsonUser.getLastName())
                .password(passwordEncoder.encode(userRegistration.getPassword()))
                .username(jsonUser.getUsername())
                .blocked(false)
                .roles(jsonUser.getRoles())
                .build();
        usersRepository.save(user);
    }

    public void updateUser(String userId, @Valid JsonUser jsonUser) {
        User user = usersRepository.find(userId);

        if (jsonUser.isBlocked() || !jsonUser.getRoles().contains(ROLE_ADMIN)) {
            if (checkExistsAnotherAdmin(user))
                throw new AtheriosException("users.cannot-delete-last-admin");
        }

        user.setFirstName(jsonUser.getFirstName());
        user.setLastName(jsonUser.getLastName());
        user.setRoles(jsonUser.getRoles());
        user.setEvidenceNumber(jsonUser.getEvidenceNumber());
        user.setBlocked(jsonUser.isBlocked());
    }

    private boolean checkExistsAnotherAdmin(User user) {
        return usersRepository.findByRole(ROLE_ADMIN).stream()
                .allMatch(admin -> admin.getUsername().equals(user.getUsername()));
    }

    public void changeUserPassword(String userId, String password) {
        User user = usersRepository.find(userId);
        user.setPassword(passwordEncoder.encode(password));
    }

    public void delete(String userId) {
        User user = usersRepository.find(userId);
        usersRepository.delete(user);
    }

}
