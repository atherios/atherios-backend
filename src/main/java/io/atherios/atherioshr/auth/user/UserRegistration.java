package io.atherios.atherioshr.auth.user;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Getter
public class UserRegistration {

    @NotNull
    @NotBlank
    @Length(min = 6)
    private final String password;

    @NotNull
    @Valid
    private final JsonUser user;

    @JsonCreator
    public UserRegistration(@JsonProperty("password") String password, @JsonProperty("user") JsonUser user) {
        this.password = password;
        this.user = user;
    }

}
