package io.atherios.atherioshr.auth.user;

import io.atherios.atherioshr.persistence.AtheriosRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsersRepository extends AtheriosRepository<User, String> {

    @Query("select distinct u from User u join u.roles r where r = :role")
    List<User> findByRole(@Param("role") UserRole role);
}
