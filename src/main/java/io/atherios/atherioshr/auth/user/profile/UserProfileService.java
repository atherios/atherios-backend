package io.atherios.atherioshr.auth.user.profile;

import io.atherios.atherioshr.auth.AtheriosAuthentication;
import io.atherios.atherioshr.auth.user.User;
import io.atherios.atherioshr.auth.user.UsersRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class UserProfileService {

    private final UsersRepository usersRepository;

    public UserInfo currentUserProfile() {
        String username = AtheriosAuthentication.currentUser().getName();
        User user = usersRepository.find(username);
        return new UserInfo(user);
    }

    public List<JsonUserProfile> allUsers() {
        return usersRepository.findAll().stream()
                .map(JsonUserProfile::new)
                .collect(Collectors.toList());
    }
}
