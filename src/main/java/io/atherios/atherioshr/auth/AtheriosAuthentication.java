package io.atherios.atherioshr.auth;

import com.google.common.collect.ImmutableSet;
import io.atherios.atherioshr.auth.user.User;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Collection;
import java.util.Objects;

public class AtheriosAuthentication implements Authentication {

    public static AtheriosAuthentication currentUser() {
        AtheriosAuthentication authentication = (AtheriosAuthentication) SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null)
            return authentication;
        return anonymous();
    }

    private static final AtheriosAuthentication ANONYMOUS = new AtheriosAuthentication("@anonymous", ImmutableSet.of()) {
        @Override
        public boolean isAuthenticated() {
            return false;
        }
    };


    public static AtheriosAuthentication anonymous() {
        return ANONYMOUS;
    }

    private final String username;
    private final Collection<? extends GrantedAuthority> authorities;

    public static AtheriosAuthentication of(User user) {
        Objects.requireNonNull(user);
        return new AtheriosAuthentication(user.getUsername(), user.getRoles());
    }

    private AtheriosAuthentication(String username, Collection<? extends GrantedAuthority> authorities) {
        this.username = username;
        this.authorities = authorities;
    }

    @Override
    public String getName() {
        return username;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public Object getCredentials() {
        return username;
    }

    @Override
    public Object getDetails() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        return null;
    }

    @Override
    public boolean isAuthenticated() {
        return true;
    }

    @Override
    public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
    }

    @Override
    public String toString() {
        return "AtheriosAuthentication[" + "username:" + username + ']';
    }
}
