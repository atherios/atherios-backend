package io.atherios.atherioshr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AtheriosHrApplication {

	public static void main(String[] args) {
		SpringApplication.run(AtheriosHrApplication.class, args);
	}
}
