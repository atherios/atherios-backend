package io.atherios.atherioshr.contract.domain.model;

import java.time.Instant;
import java.util.Date;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import org.hibernate.validator.constraints.NotBlank;

@Getter
public class JsonContract {

    @NotNull
    private final String salary;
    @NotNull
    private final String salaryString;
    private final String otherEmploymentConditions;
    @NotNull
    private final Date dateOfCommencementOfWork;
    @NotNull
    private final String periodOfWork;
    @NotNull
    private final String representedBy;
    @NotBlank
    private final String place;
    @NotNull
    private final Instant date;

    @JsonCreator
    public JsonContract(@JsonProperty("salary") String salary,
                        @JsonProperty("salary_string") String salaryString,
                        @JsonProperty("other_employment_conditions") String otherEmploymentConditions,
                        @JsonProperty("date_of_commencement_of_work") Date dateOfCommencementOfWork,
                        @JsonProperty("period_of_work") String periodOfWork,
                        @JsonProperty("represented_by") String representedBy,
                        @JsonProperty("place") String place,
                        @JsonProperty("date") Instant date) {
        this.salary = salary;
        this.salaryString = salaryString;
        this.otherEmploymentConditions = otherEmploymentConditions;
        this.dateOfCommencementOfWork = dateOfCommencementOfWork;
        this.periodOfWork = periodOfWork;
        this.representedBy = representedBy;
        this.place = place;
        this.date = date;
    }
}
