package io.atherios.atherioshr.contract.domain.repository;

import java.util.List;

import io.atherios.atherioshr.contract.domain.model.Contract;
import io.atherios.atherioshr.persistence.AtheriosRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ContractRepository extends AtheriosRepository<Contract, Integer> {

    @Query("select c from Contract c where c.candidate.id = :candidateId")
    List<Contract> findAllByCandidate(@Param("candidateId") int candidateId);

}
