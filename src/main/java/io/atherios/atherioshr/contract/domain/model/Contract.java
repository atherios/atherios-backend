package io.atherios.atherioshr.contract.domain.model;

import java.time.LocalDate;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import io.atherios.atherioshr.candidates.Candidate;
import io.atherios.atherioshr.recruitment.Recruitment;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "contracts")
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
@Getter
@Setter
public class Contract {

    @Id
    @Column(name = "contract_id")
    @GeneratedValue(generator = "contracts_gen")
    @SequenceGenerator(name = "contracts_gen", sequenceName = "contracts_seq")
    private Integer id;

    @ManyToOne
    private Candidate candidate;

    @ManyToOne
    private Recruitment recruitment;

    @Column(name = "contract_file_name", nullable = false)
    private String contractFileName;

    @Column(name = "file_date", nullable = false)
    private LocalDate fileDate;

    public Contract(Candidate candidate, Recruitment recruitment, String contractFileName, LocalDate fileDate) {
        this.candidate = candidate;
        this.recruitment = recruitment;
        this.contractFileName = contractFileName;
        this.fileDate = fileDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Contract)) return false;
        Contract contract = (Contract) o;
        return Objects.equals(getId(), contract.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
