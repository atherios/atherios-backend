package io.atherios.atherioshr.contract.domain.model;

import java.util.Date;
import javax.validation.constraints.NotNull;

import lombok.Builder;
import lombok.Getter;
import org.hibernate.validator.constraints.NotBlank;

@Getter
@Builder
public class ContractModel {

    @NotBlank
    private final String place;
    @NotNull
    private final Date date;

    @NotNull
    private final String companyName;
    @NotNull
    private final String companyAddress;
    @NotNull
    private final String representedBy;

    @NotNull
    private final String employeeName;
    @NotNull
    private final String employeeAddress;
    @NotNull
    private final String employeePesel;
    @NotNull
    private final String periodOfWork;
    @NotNull
    private final String position;

    @NotNull
    private final String salary;
    @NotNull
    private final String salaryString;
    private final String otherEmploymentConditions;
    @NotNull
    private final Date dateOfCommencementOfWork;
}
