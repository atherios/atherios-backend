package io.atherios.atherioshr.contract.domain.repository;

import io.atherios.atherioshr.contract.domain.model.ContractTemplate;
import io.atherios.atherioshr.persistence.AtheriosRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ContractTemplateRepository extends AtheriosRepository<ContractTemplate, Integer> {

    @Query("select c from ContractTemplate c where c.file.id = :id")
    ContractTemplate findByFileId(@Param("id") int id);

}
