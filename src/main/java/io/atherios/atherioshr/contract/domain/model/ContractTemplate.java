package io.atherios.atherioshr.contract.domain.model;

import io.atherios.atherioshr.files.FileDetails;
import io.atherios.atherioshr.recruitment.Recruitment;
import lombok.*;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "contract_template")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class ContractTemplate {

    @Id
    @Column(name = "contract_template_id")
    @GeneratedValue(generator = "contract_template_gen")
    @SequenceGenerator(name = "contract_template_gen", sequenceName = "contract_template_seq")
    private Integer id;

    @OneToOne
    private FileDetails file;

    public ContractTemplate(FileDetails file) {
        this.file = file;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ContractTemplate)) return false;
        ContractTemplate contractTemplate = (ContractTemplate) o;
        return Objects.equals(getId(), contractTemplate.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

}
