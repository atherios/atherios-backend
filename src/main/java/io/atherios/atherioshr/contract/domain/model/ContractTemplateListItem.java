package io.atherios.atherioshr.contract.domain.model;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import io.atherios.atherioshr.files.JsonFileDetails;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ContractTemplateListItem {

    private final int id;
    private final String link;

    @JsonUnwrapped
    private final JsonFileDetails fileDetails;

    public static ContractTemplateListItemBuilder preconfigure(ContractTemplate contractTemplate) {
        return builder()
                .id(contractTemplate.getId())
                .fileDetails(new JsonFileDetails(contractTemplate.getFile()));
    }
}
