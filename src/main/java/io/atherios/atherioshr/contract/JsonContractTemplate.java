package io.atherios.atherioshr.contract;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class JsonContractTemplate {

    private final int fileId;

    @JsonCreator
    public JsonContractTemplate(@JsonProperty("file_id") int fileId) {
        this.fileId = fileId;
    }
}
