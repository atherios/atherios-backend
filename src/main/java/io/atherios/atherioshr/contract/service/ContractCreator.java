package io.atherios.atherioshr.contract.service;

import io.atherios.atherioshr.candidates.Candidate;
import io.atherios.atherioshr.contract.domain.model.ContractModel;
import io.atherios.atherioshr.contract.domain.model.ContractTemplate;
import io.atherios.atherioshr.contract.domain.model.JsonContract;
import io.atherios.atherioshr.contract.domain.repository.ContractTemplateRepository;
import io.atherios.atherioshr.files.CompanyDetails;
import io.atherios.atherioshr.files.PdfCreator;
import io.atherios.atherioshr.recruitment.Recruitment;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.nio.file.Path;
import java.sql.Date;

@Service
public class ContractCreator {

    private final ContractTemplateRepository contractTemplateRepository;
    private final PdfCreator creator;
    private final CompanyDetails companyDetails;
    private final Path contractPath;
    private final Path contractTemplatePath;

    public ContractCreator(ContractTemplateRepository contractTemplateRepository,
                           PdfCreator creator,
                           CompanyDetails companyDetails,
                           @Value("${contract.folder.path}") Path contractPath,
                           @Value("${templates.contracts.folder.path}") Path contractTemplatePath) {
        this.contractTemplateRepository = contractTemplateRepository;
        this.creator = creator;
        this.companyDetails = companyDetails;
        this.contractPath = contractPath;
        this.contractTemplatePath = contractTemplatePath;
    }

    public File createContract(JsonContract contract, Recruitment recruitment, Candidate candidate, ContractTemplate contractTemplate) {
        ContractModel details = ContractModel.builder()
                .place(contract.getPlace())
                .date(Date.from(contract.getDate()))
                .companyName(companyDetails.getCompanyName())
                .companyAddress(companyDetails.getCompanyAddress())
                .representedBy(contract.getRepresentedBy())
                .employeeName(candidate.getFirstName() + " " + candidate.getLastName())
                .employeeAddress(candidate.getAddress())
                .employeePesel(candidate.getEvidenceNumber())
                .periodOfWork(contract.getPeriodOfWork())
                .position(recruitment.getSummary())
                .salary(contract.getSalary())
                .salaryString(contract.getSalaryString())
                .otherEmploymentConditions(contract.getOtherEmploymentConditions())
                .dateOfCommencementOfWork(contract.getDateOfCommencementOfWork())
                .build();

        String contractFilePath = contractTemplatePath + "/" + contractTemplate.getFile().getPhysicalFileName();
        return creator.createPdf(contractPath, details, contractFilePath);
    }
}
