package io.atherios.atherioshr.contract.service;

import io.atherios.atherioshr.candidates.CandidatesRepository;
import io.atherios.atherioshr.contract.RequireContractGenerationOption;
import io.atherios.atherioshr.contract.domain.model.Contract;
import io.atherios.atherioshr.contract.domain.model.ContractTemplate;
import io.atherios.atherioshr.contract.domain.model.JsonContract;
import io.atherios.atherioshr.contract.domain.repository.ContractRepository;
import io.atherios.atherioshr.contract.domain.repository.ContractTemplateRepository;
import io.atherios.atherioshr.recruitment.RecruitmentRepository;
import io.atherios.atherioshr.recruitment.candidates.RecruitedCandidate;
import io.atherios.atherioshr.recruitment.candidates.RecruitedCandidateRepository;
import io.atherios.atherioshr.recruitment.states.RecruitmentStateOptionType;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.transaction.Transactional;
import java.io.File;
import java.time.LocalDate;
import java.util.List;

@Service
@Transactional
@Validated
@AllArgsConstructor
public class ContractService {

    private final ContractRepository contractRepository;
    private final RecruitedCandidateRepository recruitedCandidateRepository;
    private final ContractCreator contractCreator;
    private final ContractTemplateRepository contractTemplateRepository;

    public List<Contract> listAll() {
        return contractRepository.findAll();
    }

    public List<Contract> listAllByCandidate(int candidateId) {
        return contractRepository.findAllByCandidate(candidateId);
    }

    public void delete(int contractId) {
        Contract contract = contractRepository.find(contractId);
        contractRepository.delete(contract);
    }

    public String createContract(JsonContract contract, int recruitmentId, int candidateId) {
        RecruitedCandidate recruitedCandidate = recruitedCandidateRepository.findByRecruitmentAndCandidate(recruitmentId, candidateId);

        int templateId = extractTemplateId(recruitedCandidate);
        ContractTemplate contractTemplate = contractTemplateRepository.find(templateId);

        File pdfFile = contractCreator.createContract(contract, recruitedCandidate.getRecruitment(), recruitedCandidate.getCandidate(), contractTemplate);

        Contract newConctract = new Contract(recruitedCandidate.getCandidate(), recruitedCandidate.getRecruitment(), pdfFile.getName(), LocalDate.now());
        contractRepository.save(newConctract);
        return pdfFile.getName();
    }

    private static int extractTemplateId(RecruitedCandidate recruitedCandidate) {
        RequireContractGenerationOption option = (RequireContractGenerationOption) recruitedCandidate.getState()
                .getOptions()
                .get(RecruitmentStateOptionType.REQUIRE_CONTRACT_GENERATION);
        return option.getContractId();
    }
}
