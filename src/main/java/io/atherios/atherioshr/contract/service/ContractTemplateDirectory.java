package io.atherios.atherioshr.contract.service;

import io.atherios.atherioshr.files.AtheriosDirectory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.nio.file.Path;

@Component
public class ContractTemplateDirectory implements AtheriosDirectory {

    private final Path directoryPath;

    public ContractTemplateDirectory(@Value("${templates.contracts.folder.path}") Path directoryPath) {
        this.directoryPath = AtheriosDirectory.createDirectory(directoryPath);
    }

    @Override
    public Path path() {
        return directoryPath;
    }
}
