package io.atherios.atherioshr.contract.service;

import io.atherios.atherioshr.contract.JsonContractTemplate;
import io.atherios.atherioshr.contract.domain.model.ContractTemplate;
import io.atherios.atherioshr.contract.domain.model.ContractTemplateListItem;
import io.atherios.atherioshr.contract.domain.repository.ContractTemplateRepository;
import io.atherios.atherioshr.files.FileDetails;
import io.atherios.atherioshr.files.FileService;
import io.atherios.atherioshr.proposal.categoryattachments.AtheriosFile;
import io.atherios.atherioshr.recruitment.states.RecruitmentStatesRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
@AllArgsConstructor
public class ContractTemplateService {

    private final FileService fileService;
    private final ContractTemplateRepository contractTemplateRepository;
    private final ContractTemplateDirectory contractTemplateDirectory;
    private final RecruitmentStatesRepository recruitmentStateRepository;

    public ContractTemplateListItem getById(int id) {
        ContractTemplate contractTemplate = contractTemplateRepository.find(id);
        return asListItem(contractTemplate);
    }

    public List<ContractTemplateUsage> findUsages(int templateId) {
        return recruitmentStateRepository.findAll().stream()
                .flatMap(state -> state.getOptions().values().stream()
                    .filter(option -> option.uses(ContractTemplate.class, templateId))
                    .map(option -> new ContractTemplateUsage(state)))
                .collect(Collectors.toList());
    }

    private static ContractTemplateListItem asListItem(ContractTemplate contractTemplate) {
        return ContractTemplateListItem.preconfigure(contractTemplate)
                .link("/contract-templates/" + contractTemplate.getId() + "/file")
                .build();
    }

    public Optional<AtheriosFile> findTemplateFile(int templateId) {
        ContractTemplate template = contractTemplateRepository.find(templateId);

        return contractTemplateDirectory.findFile(template.getFile())
                .map(templateFile -> new AtheriosFile(templateFile, template.getFile().getMime()));
    }

    public List<ContractTemplateListItem> getAll() {
        return contractTemplateRepository.findAll().stream()
                .map(ContractTemplateService::asListItem)
                .collect(Collectors.toList());
    }

    public void createRecruitmentTemplate(@Valid JsonContractTemplate jsonContractTemplate) {
        FileDetails fileDetails = fileService.moveFile(jsonContractTemplate.getFileId(), contractTemplateDirectory);
        contractTemplateRepository.save(new ContractTemplate(fileDetails));
    }

    public void delete(int templateId) {
        ContractTemplate contractTemplate = contractTemplateRepository.find(templateId);

        if (!findUsages(contractTemplate.getId()).isEmpty())
            return;

        contractTemplateDirectory.deleteFile(contractTemplate.getFile());
        contractTemplateRepository.delete(contractTemplate);
    }

}
