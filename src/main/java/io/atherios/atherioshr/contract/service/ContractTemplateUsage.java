package io.atherios.atherioshr.contract.service;

import io.atherios.atherioshr.recruitment.states.RecruitmentState;
import io.atherios.atherioshr.recruitment.states.RecruitmentStateListItem;
import lombok.Getter;

@Getter
public class ContractTemplateUsage {

    private final RecruitmentStateListItem state;

    public ContractTemplateUsage(RecruitmentState state) {
        this.state = new RecruitmentStateListItem(state);
    }
}
