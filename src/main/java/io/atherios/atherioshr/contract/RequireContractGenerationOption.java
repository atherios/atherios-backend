package io.atherios.atherioshr.contract;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.atherios.atherioshr.contract.domain.model.ContractTemplate;
import io.atherios.atherioshr.recruitment.states.RecruitmentStateOptionType;
import io.atherios.atherioshr.recruitment.states.RecruitmentStateOption;
import lombok.Getter;

@Getter
public class RequireContractGenerationOption implements RecruitmentStateOption {

    private final int contractId;

    @JsonCreator
    public RequireContractGenerationOption(@JsonProperty("contract_id") int contractId) {
        this.contractId = contractId;
    }

    @Override
    public RecruitmentStateOptionType getType() {
        return RecruitmentStateOptionType.REQUIRE_CONTRACT_GENERATION;
    }

    @Override
    public boolean uses(Class<?> resource, int id) {
        if (resource.isAssignableFrom(ContractTemplate.class))
            return contractId == id;
        return false;
    }
}
