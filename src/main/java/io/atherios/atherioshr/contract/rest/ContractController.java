package io.atherios.atherioshr.contract.rest;

import com.google.common.collect.ImmutableMap;
import io.atherios.atherioshr.contract.domain.model.Contract;
import io.atherios.atherioshr.contract.service.ContractService;
import io.atherios.atherioshr.contract.domain.model.JsonContract;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.PathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.nio.file.Path;
import java.util.List;

@RestController
@RequestMapping("/contracts")
public class ContractController {

    private final ContractService contractService;
    private final Path contractsFolder;

    public ContractController(ContractService contractService,
                              @Value("${contract.folder.path}") Path contractsFolder) {
        this.contractService = contractService;
        this.contractsFolder = contractsFolder;
    }

    @GetMapping
    public List<Contract> getListContracts() {
        return contractService.listAll();
    }

    @DeleteMapping("/{contractId}")
    public ResponseEntity delete(@PathVariable("contractId") int contractId) {
        contractService.delete(contractId);
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/recruitment/{recruitmentId}/candidates/{candidateId}")
    public Object createContract(@RequestBody @Valid JsonContract details,
                                 @PathVariable("recruitmentId") int recruitmentId,
                                 @PathVariable("candidateId") int candidateId) {

        String fileName = contractService.createContract(details, recruitmentId, candidateId);
        return ImmutableMap.of("link", "/api/contracts/" + fileName);
    }

    @GetMapping("/{filename:.*}")
    public ResponseEntity<Resource> contract(@PathVariable("filename") String filename) {
        Path contractPath = contractsFolder.resolve(filename);
        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_PDF)
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + filename + '"')
                .body(new PathResource(contractPath));
    }

}
