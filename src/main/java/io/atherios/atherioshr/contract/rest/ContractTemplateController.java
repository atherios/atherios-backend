package io.atherios.atherioshr.contract.rest;

import io.atherios.atherioshr.contract.JsonContractTemplate;
import io.atherios.atherioshr.contract.domain.model.ContractTemplateListItem;
import io.atherios.atherioshr.contract.service.ContractTemplateService;
import io.atherios.atherioshr.contract.service.ContractTemplateUsage;
import lombok.AllArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/contract-templates")
@AllArgsConstructor
public class ContractTemplateController {

    private final ContractTemplateService contractTemplateService;

    @GetMapping
    public List<ContractTemplateListItem> getListContractTemplates() {
        return contractTemplateService.getAll();
    }

    @GetMapping("/{templateId}")
    public ContractTemplateListItem getContractTemplate(@PathVariable("templateId") int templateId) {
        return contractTemplateService.getById(templateId);
    }

    @GetMapping("/{templateId}/usages")
    public List<ContractTemplateUsage> contractTemplateUsages(@PathVariable("templateId") int templateId) {
        return contractTemplateService.findUsages(templateId);
    }


    @GetMapping("/{templateId}/file")
    public ResponseEntity<Resource> templateFile(@PathVariable("templateId") int templateId) {
        return contractTemplateService.findTemplateFile(templateId)
                .map(atheriosFile -> ResponseEntity.ok()
                        .contentType(MediaType.parseMediaType(atheriosFile.getMime()))
                        .body(atheriosFile.getResource()))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping
    public void uploadContract(@RequestBody JsonContractTemplate jsonContractTemplate) {
        contractTemplateService.createRecruitmentTemplate(jsonContractTemplate);
    }

    @DeleteMapping("/{templateId}")
    public ResponseEntity delete(@PathVariable("templateId") int templateId) {
        contractTemplateService.delete(templateId);
        return ResponseEntity.noContent().build();
    }

}
