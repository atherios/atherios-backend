package io.atherios.atherioshr.notifications.tag;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

@Service
@Validated
@Transactional
public class TagService {

    private final TagRepository tagRepository;

    public TagService(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    public List<TagListItem> listAll() {
        return tagRepository.findAll()
                            .stream()
                            .filter(Objects::nonNull)
                            .map(TagListItem::new)
                            .collect(Collectors.toList());
    }

    public TagListItem findOne(int tagId) {
        Tag tag = tagRepository.findOne(tagId);
        return new TagListItem(tag);
    }


    public void create(@Valid JsonTag jsonTag) {
        Tag tag = Tag.builder()
                     .name(jsonTag.getName())
                     .build();
        tagRepository.save(tag);
    }

    public void update(int tagId, @Valid JsonTag jsonTag) {
        Tag tag = tagRepository.findOne(tagId);
        tag.setName(jsonTag.getName());

    }

    public void delete(int tagId) {
        Tag tag = tagRepository.findOne(tagId);
        tagRepository.delete(tag);
    }
}
