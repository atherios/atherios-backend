package io.atherios.atherioshr.notifications.tag;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import io.atherios.atherioshr.notifications.notification.Notification;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "tag")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Tag {

    @Id
    @Column(name = "tag_id")
    @GeneratedValue(generator = "tags_gen")
    @SequenceGenerator(name = "tags_gen", sequenceName = "tags_seq")
    private Integer id;

    @Column(name = "name", nullable = false)
    private String name;

    @ManyToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinTable(name = "notification_tag",
               joinColumns = @JoinColumn(name = "tag_id") ,
               inverseJoinColumns = @JoinColumn(name = "notification_id")
    )
    private Set<Notification> notifications;

    public Set<Notification> getNotifications() {
        if (notifications == null)
            notifications = new HashSet<>();
        return notifications;
    }

    public void setNotifications(Set<Notification> tasks) {
        getNotifications().clear();
        getNotifications().addAll(tasks);
    }

}
