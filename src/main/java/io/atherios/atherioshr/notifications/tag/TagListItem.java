package io.atherios.atherioshr.notifications.tag;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import lombok.Getter;

@Getter
public class TagListItem {

    private final Integer id;

    @JsonUnwrapped
    private final JsonTag jsonTag;

    public TagListItem(Tag tag) {
        this.id = tag.getId();
        this.jsonTag = new JsonTag(tag);
    }
}
