package io.atherios.atherioshr.notifications.tag;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import org.hibernate.validator.constraints.NotBlank;

@Getter
public class JsonTag {

    @NotNull
    @NotBlank
    private final String name;

    @JsonCreator
    public JsonTag(@JsonProperty("name") String name) {
        this.name = name;
    }

    public JsonTag(Tag tag) {
        this.name = tag.getName();
    }
}
