package io.atherios.atherioshr.notifications.tag;

import io.atherios.atherioshr.persistence.AtheriosRepository;

public interface TagRepository extends AtheriosRepository<Tag, Integer> {
}
