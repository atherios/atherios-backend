package io.atherios.atherioshr.notifications.tag;

import java.util.List;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("tag")
@AllArgsConstructor
public class TagController {

    private final TagService tagService;

    @GetMapping
    public List<TagListItem> listAll() {
        return tagService.listAll();
    }

    @GetMapping("/{tagId}")
    public TagListItem findOne(@PathVariable("tagId") int tagId) {
        return tagService.findOne(tagId);
    }

    @PostMapping
    public void save(@RequestBody JsonTag jsonTag) {
        tagService.create(jsonTag);
    }

    @PutMapping("/{tagId}")
    public void update(@PathVariable("tagId") int tagId,
                       @RequestBody JsonTag jsonTag) {
        tagService.update(tagId, jsonTag);
    }

    @DeleteMapping("/{tagId}")
    public void delete(@PathVariable("tagId") int tagId) {
        tagService.delete(tagId);
    }

}
