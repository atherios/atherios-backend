package io.atherios.atherioshr.notifications.notification;

import io.atherios.atherioshr.notifications.distribution.DistributionArea;
import io.atherios.atherioshr.notifications.distribution.NotificationDistributionListItem;
import io.atherios.atherioshr.notifications.distribution.NotificationDistributionService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/notifications")
@AllArgsConstructor
public class NotificationController {

    private final NotificationService notificationService;
    private final NotificationDistributionService notificationDistributionService;

    @GetMapping
    public List<NotificationListItem> listAll() {
        return notificationService.listAll();
    }

    @GetMapping("/user/{authorId}")
    public List<NotificationListItem> listAllByUser(@PathVariable("authorId") String authorId) {
        return notificationService.findAllByUser(authorId);
    }

    @GetMapping("/{notificationId}")
    public NotificationListItem findOne(@PathVariable("notificationId") int notificationId) {
        return notificationService.findOne(notificationId);
    }

    @PostMapping
    public void save(@RequestBody JsonNotification jsonNotification) {
        notificationService.create(jsonNotification);
    }

    @PostMapping("/{notificationId}/distribution")
    public void distributeNotification(@PathVariable("notificationId") int notificationId,
                                       @RequestBody DistributionArea distributionArea) {
        notificationDistributionService.distributeNotification(notificationId, distributionArea);
    }

    @GetMapping("/{notificationId}/distribution")
    public List<NotificationDistributionListItem> findNotificationDistributions(@PathVariable("notificationId") int notificationId) {
        return notificationDistributionService.findNotificationDistributions(notificationId);
    }

    @PutMapping("/{notificationId}/tags/{tagId}")
    public void addTag(@PathVariable("tagId") int tagId,
                       @PathVariable("notificationId") int notificationId) {
        notificationService.addTag(tagId, notificationId);
    }

    @DeleteMapping("/{notificationId}/tags/{tagId}")
    public void removeTag(@PathVariable("tagId") int tagId,
                          @PathVariable("notificationId") int notificationId) {
        notificationService.removeTag(tagId, notificationId);
    }

    @PutMapping("/{notificationId}")
    public void update(@PathVariable("notificationId") int notificationId,
                       @RequestBody JsonNotification jsonNotification) {
        notificationService.update(notificationId, jsonNotification);
    }

    @DeleteMapping("/{notificationId}")
    public void delete(@PathVariable("notificationId") int notificationId) {
        notificationService.delete(notificationId);
    }
}
