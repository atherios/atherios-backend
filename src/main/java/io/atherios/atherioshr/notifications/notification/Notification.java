package io.atherios.atherioshr.notifications.notification;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import io.atherios.atherioshr.auth.user.User;
import io.atherios.atherioshr.notifications.tag.Tag;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "notification")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Notification {

    @Id
    @Column(name = "notification_id")
    @GeneratedValue(generator = "notifications_gen", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "notifications_gen", sequenceName = "notifications_seq")
    private Integer id;

    @Column(name = "title", nullable = false, length = 64)
    private String title;

    @Column(name = "content", nullable = false, length = 4098)
    private String content;

    @Column(name = "priority")
    private int priority;

    @ManyToOne(optional = false)
    @JoinColumn(name = "user_id")
    private User author;

    @ManyToMany(mappedBy = "notifications")
    private Set<Tag> tags;

    public Set<Tag> getTags() {
        if (tags == null)
            tags = new HashSet<>();
        return tags;
    }

    public void setTags(List<Tag> tags) {
        getTags().clear();
        getTags().addAll(tags);
    }
}
