package io.atherios.atherioshr.notifications.notification;

import com.google.common.primitives.Ints;
import io.atherios.atherioshr.lucene.AtheriosDocument;
import io.atherios.atherioshr.lucene.AtheriosDocuments;
import io.atherios.atherioshr.lucene.index.IndexFields;
import io.atherios.atherioshr.lucene.LuceneDocumentBuilder;
import io.atherios.atherioshr.notifications.distribution.NotificationDistribution;
import io.atherios.atherioshr.notifications.distribution.NotificationDistributionRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@Component
@AllArgsConstructor
public class NotificationDocuments implements AtheriosDocuments {


    private final NotificationRepository notificationRepository;
    private final NotificationDistributionRepository notificationDistributionRepository;

    @Override
    public Stream<AtheriosDocument> allDocuments() {
        return notificationRepository.findAll().stream()
                .map(this::asAtheriosDocument);
    }

    private AtheriosDocument asAtheriosDocument(Notification notification) {
        LuceneDocumentBuilder documentBuilder = LuceneDocumentBuilder.builder(Notification.class, notification.getId())
                .withField(IndexFields.TEXT_SEARCH, notification.getTitle())
                .withField(IndexFields.TEXT_SEARCH, notification.getContent())
                .withField(IndexFields.STRINGS_SEARCH, notification.getAuthor().getUsername());
        List<NotificationDistribution> distributions = notificationDistributionRepository.findByNotification(notification.getId());

        distributions
                .forEach(distribution -> documentBuilder.withField(IndexFields.MINOR_STRINGS_SEARCH, distribution.getTargetUser().getUsername()));

        return documentBuilder.build();
    }

    @Override
    public Optional<Object> findSearchHit(String id) {
        return notificationRepository.tryFind(Ints.tryParse(id))
                .map(NotificationListItem::new);
    }

    @Override
    public String documentType() {
        return Notification.class.getCanonicalName();
    }

    @Override
    public Optional<AtheriosDocument> findIndexDocument(String entityId) {
        return notificationRepository.tryFind(Integer.parseInt(entityId)).map(this::asAtheriosDocument);

    }
}
