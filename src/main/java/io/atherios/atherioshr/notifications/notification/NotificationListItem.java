package io.atherios.atherioshr.notifications.notification;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import io.atherios.atherioshr.auth.user.profile.JsonUserProfile;
import lombok.Getter;

@Getter
public class NotificationListItem {

    private final Integer id;

    @JsonUnwrapped
    private final JsonNotification jsonNotification;
    private final JsonUserProfile author;

    public NotificationListItem(Notification notification) {
        this.id = notification.getId();
        this.jsonNotification = new JsonNotification(notification);
        this.author = new JsonUserProfile(notification.getAuthor());
    }
}
