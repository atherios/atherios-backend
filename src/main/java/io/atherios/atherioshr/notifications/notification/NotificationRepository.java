package io.atherios.atherioshr.notifications.notification;

import java.util.List;

import io.atherios.atherioshr.auth.user.User;
import io.atherios.atherioshr.persistence.AtheriosRepository;
import org.springframework.data.jpa.repository.Query;

public interface NotificationRepository extends AtheriosRepository<Notification, Integer> {


    List<Notification> findAllByAuthor(User user);
}
