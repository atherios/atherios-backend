package io.atherios.atherioshr.notifications.notification;

import java.util.List;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import javax.validation.Valid;

import io.atherios.atherioshr.auth.AtheriosAuthentication;
import io.atherios.atherioshr.auth.user.User;
import io.atherios.atherioshr.auth.user.UsersRepository;
import io.atherios.atherioshr.lucene.DocumentChangedEvent;
import io.atherios.atherioshr.lucene.search.DocumentIdentity;
import io.atherios.atherioshr.notifications.distribution.NotificationDistribution;
import io.atherios.atherioshr.notifications.distribution.NotificationDistributionRepository;
import io.atherios.atherioshr.notifications.tag.Tag;
import io.atherios.atherioshr.notifications.tag.TagRepository;
import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

@Service
@Transactional
@Validated
@AllArgsConstructor
public class NotificationService {

    private final NotificationRepository notificationRepository;
    private final NotificationDistributionRepository notificationDistributionRepository;
    private final UsersRepository usersRepository;
    private final TagRepository tagRepository;
    private final ApplicationEventPublisher applicationEventPublisher;

    public List<NotificationListItem> listAll() {
        return notificationRepository.findAll().stream()
                .map(NotificationListItem::new)
                .collect(Collectors.toList());
    }

    public NotificationListItem findOne(int taskId) {
        Notification notification = notificationRepository.find(taskId);
        return new NotificationListItem(notification);
    }

    public List<NotificationListItem> findAllByUser(String userId) {
        User user = usersRepository.find(userId);

        return notificationRepository.findAllByAuthor(user).stream()
                .map(NotificationListItem::new)
                .collect(Collectors.toList());
    }

    public void create(@Valid JsonNotification jsonNotification) {

        Notification notification = Notification.builder()
                .title(jsonNotification.getTitle())
                .content(jsonNotification.getContent())
                .priority(jsonNotification.getPriority())
                .author(loggedUser())
                .build();

        notificationRepository.save(notification);
        applicationEventPublisher.publishEvent(new DocumentChangedEvent(new DocumentIdentity(Notification.class, notification.getId())));
    }

    private User loggedUser() {
        AtheriosAuthentication loggedUser = (AtheriosAuthentication) SecurityContextHolder.getContext().getAuthentication();
        String userName = loggedUser.getName();
        return usersRepository.find(userName);
    }

    public void update(int notificationId, @Valid JsonNotification jsonNotification) {
        Notification notification = notificationRepository.findOne(notificationId);

        notification.setContent(jsonNotification.getContent());
        notification.setTitle(jsonNotification.getTitle());
        notification.setPriority(jsonNotification.getPriority());
        applicationEventPublisher.publishEvent(new DocumentChangedEvent(new DocumentIdentity(Notification.class, notification.getId())));
    }

    public void delete(int notificationId) {
        Notification notification = notificationRepository.findOne(notificationId);
        for (Tag tag : notification.getTags()) {
            notification.getTags().remove(tag);
            tag.getNotifications().remove(notification);
        }

        List<NotificationDistribution> distributionsByNotification = notificationDistributionRepository.findByNotification(notificationId);
        notificationDistributionRepository.delete(distributionsByNotification);

        notificationRepository.delete(notification);
        applicationEventPublisher.publishEvent(new DocumentChangedEvent(new DocumentIdentity(Notification.class, notification.getId())));
    }

    public void addTag(int tagId, int notificationId) {
        Notification notification = notificationRepository.findOne(notificationId);
        Tag tag = tagRepository.find(tagId);

        notification.getTags().add(tag);
        tag.getNotifications().add(notification);
        applicationEventPublisher.publishEvent(new DocumentChangedEvent(new DocumentIdentity(Notification.class, notification.getId())));
    }

    public void removeTag(int tagId, int notificationId) {
        Notification notification = notificationRepository.findOne(notificationId);
        Tag tag = tagRepository.find(tagId);

        notification.getTags().remove(tag);
        tag.getNotifications().remove(notification);
        applicationEventPublisher.publishEvent(new DocumentChangedEvent(new DocumentIdentity(Notification.class, notification.getId())));
    }
}
