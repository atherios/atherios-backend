package io.atherios.atherioshr.notifications.notification;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import org.hibernate.validator.constraints.NotBlank;

@Getter
public class JsonNotification {

    @NotNull
    @NotBlank
    private String title;

    @NotNull
    @NotBlank
    private final String content;

    private int priority;


    @JsonCreator
    public JsonNotification(@JsonProperty("title") String title,
                            @JsonProperty("content") String content,
                            @JsonProperty("priority") int priority) {
        this.title = title;
        this.content = content;
        this.priority = priority;
    }

    public JsonNotification(Notification notification) {
        this.title = notification.getTitle();
        this.content = notification.getContent();
        this.priority = notification.getPriority();
    }

}
