package io.atherios.atherioshr.notifications.distribution;

import io.atherios.atherioshr.persistence.AtheriosRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface NotificationDistributionRepository extends AtheriosRepository<NotificationDistribution, Integer> {

    @Query("select d from NotificationDistribution d where d.targetUser.id = :recipientId")
    List<NotificationDistribution> findByRecipient(@Param("recipientId") String recipientId);

    @Query("select d from NotificationDistribution d where d.notification.id = :notificationId")
    List<NotificationDistribution> findByNotification(@Param("notificationId") int notificationId);
}
