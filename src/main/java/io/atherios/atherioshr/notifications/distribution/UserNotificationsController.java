package io.atherios.atherioshr.notifications.distribution;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
public class UserNotificationsController {

    private final NotificationDistributionService notificationDistributionService;

    @GetMapping("/users/current/notifications")
    public List<NotificationDistributionListItem> findUserNotifications() {
        return notificationDistributionService.findUserNotifications();
    }

}
