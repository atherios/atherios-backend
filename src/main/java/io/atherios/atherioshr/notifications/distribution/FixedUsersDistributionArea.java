package io.atherios.atherioshr.notifications.distribution;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

import java.util.List;

@Getter
public class FixedUsersDistributionArea implements DistributionArea {

    private final List<String> userIds;

    @JsonCreator
    public FixedUsersDistributionArea(@JsonProperty("user_ids") List<String> userIds) {
        this.userIds = userIds;
    }

    @Override
    public DistributionAreaType getType() {
        return DistributionAreaType.FIXED;
    }
}
