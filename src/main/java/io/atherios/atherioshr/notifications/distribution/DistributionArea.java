package io.atherios.atherioshr.notifications.distribution;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.EXISTING_PROPERTY,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = AllUsersDistributionArea.class, name = "ALL_USERS"),
        @JsonSubTypes.Type(value = FixedUsersDistributionArea.class, name = "FIXED")
})
public interface DistributionArea {

    DistributionAreaType getType();

}
