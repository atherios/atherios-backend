package io.atherios.atherioshr.notifications.distribution;

import com.fasterxml.jackson.annotation.JsonCreator;

public class AllUsersDistributionArea implements DistributionArea {

    private static final AllUsersDistributionArea INSTANCE = new AllUsersDistributionArea();

    @JsonCreator
    public static AllUsersDistributionArea of() {
        return INSTANCE;
    }

    private AllUsersDistributionArea() {}

    @Override
    public DistributionAreaType getType() {
        return DistributionAreaType.ALL_USERS;
    }
}
