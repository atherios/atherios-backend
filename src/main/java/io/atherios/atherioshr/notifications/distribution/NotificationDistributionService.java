package io.atherios.atherioshr.notifications.distribution;

import io.atherios.atherioshr.auth.AtheriosAuthentication;
import io.atherios.atherioshr.auth.user.User;
import io.atherios.atherioshr.auth.user.UsersRepository;
import io.atherios.atherioshr.notifications.notification.Notification;
import io.atherios.atherioshr.notifications.notification.NotificationRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.transaction.Transactional;
import java.time.Instant;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@Validated
@AllArgsConstructor
public class NotificationDistributionService {

    private static final Comparator<NotificationDistribution> DISTRIBUTION_COMPARATOR = Comparator.comparing(NotificationDistribution::getTime).reversed();

    private final NotificationDistributionRepository notificationDistributionRepository;
    private final NotificationRepository notificationRepository;
    private final DistributionAreas distributionAreas;
    private final UsersRepository usersRepository;

    public void distributeNotification(int notificationId, DistributionArea distributionArea) {
        Notification notification = notificationRepository.find(notificationId);
        List<User> recipients = distributionAreas.expand(distributionArea);

        User author = loggedUser();

        List<NotificationDistribution> distributions = recipients.stream()
                .map(recipient -> NotificationDistribution.builder()
                        .notification(notification)
                        .targetUser(recipient)
                        .author(author)
                        .time(Instant.now())
                        .build())
                .collect(Collectors.toList());

        notificationDistributionRepository.save(distributions);
    }

    public List<NotificationDistributionListItem> findUserNotifications() {
        User user = loggedUser();

        return notificationDistributionRepository.findByRecipient(user.getUsername()).stream()
                .sorted(DISTRIBUTION_COMPARATOR)
                .map(NotificationDistributionListItem::new)
                .collect(Collectors.toList());
    }

    private User loggedUser() {
        AtheriosAuthentication loggedUser = (AtheriosAuthentication) SecurityContextHolder.getContext().getAuthentication();
        String userName = loggedUser.getName();
        return usersRepository.find(userName);
    }

    public List<NotificationDistributionListItem> findNotificationDistributions(int notificationId) {
        return notificationDistributionRepository.findByNotification(notificationId).stream()
                .sorted(DISTRIBUTION_COMPARATOR)
                .map(NotificationDistributionListItem::new)
                .collect(Collectors.toList());
    }
}
