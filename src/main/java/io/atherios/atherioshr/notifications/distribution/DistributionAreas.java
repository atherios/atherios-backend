package io.atherios.atherioshr.notifications.distribution;

import com.google.common.collect.ImmutableList;
import io.atherios.atherioshr.auth.user.User;
import io.atherios.atherioshr.auth.user.UsersRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@AllArgsConstructor
public class DistributionAreas {

    private final UsersRepository usersRepository;

    public List<User> expand(DistributionArea distributionArea) {
        if (distributionArea.getType() == DistributionAreaType.ALL_USERS) {
            return usersRepository.findAll();
        }

        if (distributionArea.getType() == DistributionAreaType.FIXED) {
            FixedUsersDistributionArea area = (FixedUsersDistributionArea) distributionArea;
            return usersRepository.findAll(area.getUserIds());
        }

        return ImmutableList.of();
    }
}
