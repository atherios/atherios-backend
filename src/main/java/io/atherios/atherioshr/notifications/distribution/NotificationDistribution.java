package io.atherios.atherioshr.notifications.distribution;

import io.atherios.atherioshr.auth.user.User;
import io.atherios.atherioshr.notifications.notification.Notification;
import lombok.*;

import javax.persistence.*;
import java.time.Instant;
import java.util.Objects;

@Entity
@Table(name = "notification_distribution")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class NotificationDistribution {

    @Id
    @Column(name = "notification_distribution_id")
    @GeneratedValue(generator = "notification_distributions_gen", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "notification_distributions_gen", sequenceName = "notification_distributions_seq")
    private Integer id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "notification_id")
    private Notification notification;

    @ManyToOne(optional = false)
    @JoinColumn(name = "distributed_to")
    private User targetUser;

    @ManyToOne(optional = false)
    @JoinColumn(name = "distributed_by")
    private User author;

    @Column(name = "distribution_timestamp")
    private Instant time;


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof NotificationDistribution)) {
            return false;
        }
        NotificationDistribution that = (NotificationDistribution) o;
        return Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
