package io.atherios.atherioshr.notifications.distribution;

import io.atherios.atherioshr.auth.user.profile.JsonUserProfile;
import io.atherios.atherioshr.notifications.notification.JsonNotification;
import lombok.Getter;

import java.time.Instant;

@Getter
public class NotificationDistributionListItem {

    private final JsonUserProfile recipient;
    private final JsonUserProfile author;
    private final JsonNotification notification;
    private final Instant time;

    public NotificationDistributionListItem(NotificationDistribution distribution) {
        this.recipient = new JsonUserProfile(distribution.getTargetUser());
        this.author = new JsonUserProfile(distribution.getAuthor());
        this.notification = new JsonNotification(distribution.getNotification());
        this.time = distribution.getTime();
    }
}
