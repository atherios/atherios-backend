package io.atherios.atherioshr.mvc;

import com.google.common.base.CaseFormat;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Streams;
import io.atherios.atherioshr.AtheriosException;
import io.atherios.atherioshr.persistence.EntityNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import javax.validation.Path;
import java.util.Map;
import java.util.stream.Collectors;


@RestControllerAdvice
@Slf4j
public class GlobalAdvice {


    private static final char EMPTY_CHAR = (char) 0x0;

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<?> validationError(ConstraintViolationException e) {
        Map<String, String> json = e.getConstraintViolations().stream()
                .collect(Collectors.toMap(violation -> Streams.stream(violation.getPropertyPath())
                        .map(Path.Node::getName)
                        .skip(2)
                        .collect(Collectors.joining(".")), violation -> violationKey(violation.getMessageTemplate())));

        return ResponseEntity.badRequest().body(json);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> unknownError(Exception e) {
        log.error("Server error: {}", e.getMessage(), e);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(ImmutableMap.of(
                        "message", Strings.nullToEmpty(e.getMessage())
                ));
    }

    @ExceptionHandler(AtheriosException.class)
    public ResponseEntity<?> atheriosError(Exception e) {
        log.error("Server error: {}", e.getMessage(), e);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(ImmutableMap.of(
                        "message", Strings.nullToEmpty(e.getMessage())
                ));
    }

    @ExceptionHandler(AtheriosException.class)
    public ResponseEntity<?> entityNotFound(EntityNotFoundException e, HttpServletRequest request) {
        Map<String, String> body = ImmutableMap.of("message", Strings.nullToEmpty(e.getMessage()));
        if (HttpMethod.GET.matches(request.getMethod())) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body(body);
        }
        return ResponseEntity.badRequest()
                .body(body);
    }

    private static String violationKey(String messageTemplate) {
        return CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_HYPHEN, messageTemplate)
                .replace("{org.hibernate.", "")
                .replace(".-", ".")
                .replace('}', EMPTY_CHAR)
                .trim();
    }

}
