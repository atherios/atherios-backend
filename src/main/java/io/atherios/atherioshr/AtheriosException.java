package io.atherios.atherioshr;

public class AtheriosException extends RuntimeException {
    private final String message;

    public AtheriosException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
